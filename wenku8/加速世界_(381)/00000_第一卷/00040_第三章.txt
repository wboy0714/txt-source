春雪猛然睜開眼睛。

順著從窗口射進的白色光線望向時鐘，就看到時針指著上午六點半，算來自己竟然一睡就是將近十二小時。

他睡得全身大汗，汗水仿彿就像惡夢殘渣所化成的黏膩液體裹在皮膚上，但他卻又絲毫想不起自己夢到了什麼。

昨天黑雪公主最後所說的那句話模糊地在腦中重現。

她吩咐自己整夜都別卸下神經連結裝置。難道說這個吩咐跟自己做的夢有關?

春雪邊發呆思索邊衝完澡，換好制服後，就一個人到廚房吃由五谷片跟柳橙汁組成的早餐。接著將餐具塞進洗碗機，再去敲母親寢室的房門，準備完成每天上學前都要進行的儀式。

「……我上學去了。」

他朝著昏暗的室內說了一聲後，就隱約聽到床上傳來一陣微弱的聲響。看樣子她昨天晚上喝了很多。

春雪等著母親操作手邊的終端機，將五百圓儲值到自己的神經連結裝置上，但母親說話的語調卻忽然變得不耐煩：

「春雪，你根本沒連線。」

春雪暗叫糟糕，趕忙將手伸向脖子上。儘管覺得自己似乎忘了什麼事，但還是將神經連結裝置接上全球網絡，緊接著就聽見喀啷一聲音效響起，電子貨幣的金額上加了一筆。

「我上學去了。」

春雪又說了一次，但母親已經沒有響應。他輕輕關上寢室的門，在玄關口換上運動鞋，走出了家門。

坐電梯下到一樓，對一群連長相都不太記得住的居民含糊打過招呼後，穿越了入口大廳。

就在他通過自動門，一腳踩進公寓前庭的短短三秒鐘之後。

啪的一聲，聲響蓋過了春雪的腦袋。

整個世界當場變暗。在朝陽下閃閃發光的街景，一瞬間就變得夜幕低垂。

怎麼回事?是「加速」?可是——為什麼程序會自己啟動?

就在倒吸一口氣的春雪眼前，一排熟悉的燃燒字體排出了一串字母。

【HERE COMES A NEW CHALLENGER！！】

總覺得這句訊息似曾相識，但還來不及去翻找自己的記憶，火焰文字就已經燃燒殆盡，視野上方出現了更令他覺得不可思議的對象。

首先是中央有著一排數字顯示著【1800】，數字左右各有一條藍色色條往外筆直延伸，兩條色條下方又各有一條較細的綠色色條。

最後則是一排火焰文字顯示於視野中央——【FIGHT!!】

數字變成了1799。

春雪不知所措，好一陣子都只愣愣注視著四位數的數字倒數。

一千八百秒，也就是三十分鐘。總覺得自己才剛聽過這個數字。沒錯——黑雪公主說過的最長「加速」時間，不就是這個長度嗎?

可是這次春雪連加速指令「超頻連線」的「超」字都沒說出口，眼前世界的顏色也不像上次是一片藍色，而是保留全彩。而且真要說起來，那些什麼CHALLENGER還是FIGHT之類的字樣更讓他莫名其妙。

春雪拚命環顧周遭，想盡量掌握狀況，接著立即發現了一件事。

十月爽朗的朝陽已消失得無影無踪，但四周的地形卻跟已深深印於記憶中的自家前景象一模一樣。有四線道的道路，道路對面有著成排的便利商店與辦公大樓，回過頭更能見到那棟仿彿才剛出現般的高層公寓大樓，高聳的身影在黑暗中幾乎直達天際。

然而本來應該在通往新宿方面的車道上擠得水泄不通的車流，以及應該占滿整條人行道的上班上學人潮，瞬間不見踪影。不僅如此，道路四處出現龜裂或下陷的情形，護欄跟交通號誌也都歪七扭八，建築物的玻璃更是破得亂七八糟。

一個不遠處的路口，還可看到斷垣一殘壁堆得像一堆路障，更有猛烈燃燒的火焰從巨大汽油桶裡竄起。這些破壞的痕跡在春雪所住的公寓大樓也非常嚴重，不但水泥支柱崩塌，外牆也開出大洞，災情十分慘重。

一股想要馬上折回家裡查看情形的衝動，驅使春雪搖搖晃晃地走了幾步，從斷垣殘壁之間窺探入口內的情形。

接著他立刻看得目瞪口呆。建築物內部簡直就像一頭鑽進遊戲裡多邊形建築物時會看到的光景，只看到一種表面沒有任何凹凸起伏或圖案的灰色平面鋪成箱狀。不對，不是好像，而是眼前就是這種景象。

這裡既是現實，又非現實。春雪現在正以「加速」功能完全沉潛到虛擬實境網絡中，周圍的光景都是由公共攝影機所拍到的畫面重新建構而成的3D影像，就如同昨天在交誼廳裡看到的那個凍結成一片藍色的世界一樣。

只是話說回來，春雪從來沒有看過這麼精緻的虛擬空間，根本看不出畫素到底有多密。就連腳邊隨便一顆小石子都極為精細，細節可說是壓倒性的清晰。

那麼自己的身體又是什麼模樣呢?想到這裡，春雪就低頭看看自己。

他原本以為會看到熟悉的粉紅豬虛擬角色，然而——

「……這……是什麼玩意?」

結果卻看得目瞪口呆，忍不住發出驚嘆聲。

映入眼簾的，是一具不分腳、軀幹、還是雙手都有如鐵絲般纖細，整副身軀就像打磨過的銀器一樣光亮，簡直像個機器人——但卻絲毫沒有遊戲或動畫中常見的戰斗性色彩。

春雪趕忙伸手往臉上一摸，卻沒有摸到鼻子或嘴巴，堅硬的指頭只在一面宛如安全帽般平滑的曲面上滑過。他趕忙環顧四周，在公寓大樓馬路對面的一棟混居公寓牆面上，找到一扇打破的玻璃窗，隨後立刻踩著鏗鏘作響的腳步聲跑了過去。

大玻璃窗上映照出的身影，無庸置疑的是一具全身覆滿金屬的機器人。極為瘦小的軀體上，只有流線型的頭部大得突兀。如果要用一句話來形容，就是個小咖。

至少額頭上也長根角，再不然就是兩只眼睛發個金光吧。

就在春雪對不知名的虛擬角色設計師這麼抱怨時，看到了玻璃裡自己的身後，有好幾道人影動來動去。

他顫抖著這副金屬身體轉過身，也不知道這些人是什麼時候出現的，只見遭到破壞的便利商店屋檐下，有三道人影正看著自己。由於對方身在暗處，只隱約看得出輪廓，但可確定每個人都比春雪高大得多。

三道人影臉湊在一起，似乎在討論事情。春雪忍不住仔細傾聽。

「……總覺得這小子好像很膽小啊。」

「也沒有印象聽過這個名字啊，他是初學者嗎?」

「可是他是金屬色耶，應該會有點本事吧?」

這些一傢伙不是NPC（注：Non-Player Character的簡稱，泛指遊戲中並非由玩家控制的角色）。

春雪的直覺察覺到了這一點。光看態度跟口吻就知道，他們顯然都不是程序，而是活生生的人類。

可是這裡是經過「加速」的虛擬網絡。這也就表示他們都跟自己以及黑雪公主一樣，已經安裝了BRAIN BURST程序。

那麼他們應該會知道這個狀況是怎麼回事吧?春雪心想：還是先找他們三個問清楚再說。於是他戰戰兢兢地踏上大馬路，朝中央的白線前進。

忽然間，他又發現了新的視線與聲息。春雪停下腳步，讓視線掃過四周。

這裡不只那三個人而已，還有別人。也不知道這些人是從哪裡出現的，舉凡廢棄大樓的屋頂、大堆斷垣一殘壁的頂端，各個方位都有許多輪廓奇特的人物看著春雪。可是他們倒也沒有更加接近，模樣看起來就只是在——對了，就是在等待。

春雪束手無策地站在道路正中央，僅移動視線觀看。不知不覺間，視野上方的倒數數字已經減少到1620，數字左右兩邊各兩條的色條則沒有改變——

不對，先前他沒有發現，但其實左右色條下面各有一串小小的英文字母。

左側的文字寫著「Silver Crow」，而右側則是「Ash Roller」。

這個畫面配置讓他覺得很眼熟，而且是非常眼熟。

春雪在一陣強烈的似曾相識感中有了這個想法。

這個設計一點都不新——是某種比春雪還早了三十年以上誕生，于一九○○年代末期席捲日本各地游樂場的遊戲程序。總覺得自己最近也曾經看過某個景象，因而想起了那種遊戲程式。記得是在……

春雪愣愣站著不動，翻找著自己的記憶，突然間背後傳來一聲爆響，嚇得他跳了起來。

「……?」

就在春雪想要轉身卻失去平衡坐倒在地時，一抹特別巨大的輪廓已經屹立在他眼前。

那是一輛機車，且並非常見的電力馬達驅動型……而是以很久以前就立法禁止的內燃機作為心臟，正發出沉重的震動聲響。

前輪架長得簡直不可理喻，夾在輪架中間的輪胎也大得像是在開玩笑，厚實的灰色輪胎踏面還不時飄散出微微的焦臭味。

春雪將視線往上一拉，戰戰兢兢地在彎得十分誇張的握把對面，捕捉到了一名跨坐在皮革座墊上的騎士。

這名騎士全身穿著打上鉚釘的黑色皮衣，腳上的皮靴穩穩踏在地上，雙手環抱在胸前。這個人的頭部也戴著黑色安全帽，但面罩則是非常搶眼的骷髏造型。

春雪茫然地聽見從安全帽中發出一道尖銳的嗓音：

「好久沒有來到『世紀末』場地啦，運氣有夠好的啦啦啦啦啦～」

他環抱在胸前的一隻手還伸出食指左右搖動。

「而且對手還是亮晶晶的新手，運氣超贊的啦啦啦啦啦——」

骷髏騎士抬起穿著皮靴的右腳，放到踏杆上靈活地刷過，接著就聽到一聲轟隆巨響，讓春雪再次跳了起來。

怎麼看都不覺得這個人態度友善。別說友善了，要是先前的聯想沒有錯，這裡是「對戰場地」——而這名機車騎士則是——

「哇……哇啊……」

春雪一寸一寸往後挪，轉過身去。

「哇——」

接著拚命踩響了機器人般的纖細雙腳往前跑。

「嘻哈哈哈哈哈!跑啊!快跑啊!」

引擎又在背後發出咆哮，接著是一陣魔音穿腦般的輪胎空轉聲——過了短短一秒鐘，背上傳來一股極為強烈的衝擊與悶痛，春雪整個人在夜空中高高飛起。

同時視野左上方「Silver Crow」這邊的藍色色條瞬間縮減了許多。

看到這個情形，在空中轉了好幾圈的春雪不禁心想：果然沒錯。

也就是說，這是一場「對戰遊戲」，自己是個一竅不通的初學者，而對方則是熱門熟路的老手。

想也知道不可能會贏。

『哈哈哈，這麼快就被痛宰啦?這可都是因為你沒遵守跟我的約定啊，少年。』

午休時間。

在交誼廳跟春雪進行直連的黑雪公主就和昨天一樣，晃著瀏海下貼有促進治療貼布的頭，靈巧地只用思念發出笑聲。照她的說法，傷口雖然出血很多，但頂多只是裂傷。而春雪翻遍了所有自己懂得的字彙準備了道謝與道歉的話，卻被她輕輕一揮手擋了下來。

『這……這一點都不好笑，我差點以為自己會沒命耶……當然如果真要追究責任，還是該怪我自己不小心把神經連結裝置連到全球網絡上啦……』

黑雪公主以愉快的神情看著春雪嘀嘀咕咕的模樣，從桌上端起茶杯碰了碰嘴唇。一旁擺著一盤沒有碰過的鮮蝦焗飯，和春雪面前的大盤豬肉咖哩飯同樣冒著熱氣。

坐在同桌的幾個學生會幹部已經開始大動筷子跟湯匙，春雪的胃也發出了沒出息的聲音，但黑雪公主的授課——或者該說是訓話，看來卻沒有這麼快結束。

『——不過也好，這一來我也省了不少唇舌，不用一一跟你說明清楚。雖然學費貴了點，不過這下你應該也搞懂了吧?』

『搞懂……什麼事情?』

『搞懂「BRAIN BURST」到底是什麼樣的程序。這不是什麼大手筆的陰謀，只是個——』

春雪無力地點了點頭，思緒中浮現一個詞彙，接過黑雪公主停住的話頭：

『只是個對戰格闘遊戲，而且還是以現實世界作為舞台進行遭遇戰，真是離譜……』

『呵呵，的確很離譜，很讓人頭痛啊。』

『我還想說能「加速」思考的這種超高科技到底是要來做什麼，沒想到竟然是搞對戰格闘!這個遊戲類別不是都已經荒廢三十年以上了嗎!』

聽他這麼一說，黑雪公主歪著頭想了一想，臉上露出了略帶諷刺的笑容說：

『嗯——春雪，你這個說法有點偏頗。我們超頻連線者並非為了玩格闘遊戲才「加速」，事實上正好相反，反而是為了維持「加速」的能力而進行對戰。我們不得不如此，而這正是這個程序最讓人恨得牙癢癢的地方。』

『這話……怎麼說?』

『嗯……接下來的部分應該還是到現場去說明比較好吧?你「加速」一下。』

『哦、哦……』

春雪斬斷對大盤咖哩飯的眷戀，乖乖從椅子上端正坐姿，在口中喊出加速指令：

超頻連線!

熟悉的火花聲撼動身體與意識，周圍的學生立刻停住了動作，同時失去所有色彩，換上一片具透明感的藍色。

眼前的黑雪公主也同樣停住動作，但隨即就看到身穿妖艷漆黑禮服的虛擬角色，從清純的制服身影上仿彿靈魂出竅般地站起。春雪也以粉紅豬的虛擬角色從椅子上跳了下來，往前站上一步，不讓現實世界中自己圓滾滾的身體映入眼簾。

「那……我們要怎麼做?」

「你的視野左側有沒有多出新的圖示?」

春雪依言轉動視線，就看到排列在左側的應用程序啟動圖標中，新增了一個燃燒的B字樣圖示。春雪舉起左手點選圖示。

「這就是對戰格闘遊戲軟件『BRAIN BURST』的選單畫面。可以查看自己的能力畫面、戰績，還可以搜尋周圍的超頻連線者來進行挑戰。你按一下安排對戰的按鈕看看。」

春雪點點頭，點選了位於選單最底下的按鈕，緊接著就開出一個新的窗口，短短地顯示一下搜尋字樣後，名單就接著出現。

雖然說是名單，但是上面只有兩個名字。一個是今天早上也有看到，應該就是代表自己的「Silver Crow」——另一個名字則是「Black Lotus」。

春雪絲毫沒有懷疑這就是黑雪公主化為超頻連結者時的名號，但他還是抬起頭來瞥了她一眼，想問清楚是否真的沒有錯。而黑雪公主的虛擬角色也如他所想地輕輕點了點頭說：

「現在我們沒有連上全球網絡，只和校內局域網絡連線，所以名單上只有你跟我——照理說應該是這樣。」

「是……Black Lotus小姐。」

春雪很想說些這名字真美或是真適合妳之類的讚美，但他當然沒能流暢地發出聲來，結果只是動了動豬鼻子而已。

「好，那你就點選我的名字，找我進行對戰。」

「咦……咦咦?」

「我又不是真的要跟你打，拖到時間結束就會變成平手了。」

黑雪公主微微苦笑，催春雪趕快照做。

儘管他覺得這年頭能容納數萬名玩家於同一個戰場上廝殺的大規模戰鬥遊戲，早已不稀奇，幹嘛搞什麼一對一。但春雪還是輕輕點選名單上的名字，從跳出的選單裡選擇【DUEL】，並從接著跳出的YES／NO對話框裡點選【YES】。

這一瞬間，世界的樣貌再度產生改變。

所有學生都一起從靜止不動的藍色交誼廳中消失，柱子跟桌子等等的對象雖然慢慢恢復顏色，但同時又像風化般不斷腐朽，玻璃也積上了厚厚的灰塵。

接著天空也染上一片極為深邃的橋紅色。一陣乾燥的風吹過，吹動了從地板上各處冒出的不知名野草。

啪的一聲，熟悉的數字1800立刻進入視野上方，藍色色條往左右延伸，最後出現的則是一串寫著【FIGHT!!】的火焰文字。

「哦……?是『黃昏』?這下可抽到了挺稀奇的場地。」

黑雪公主說話的聲音從四處張望的春雪身旁響起。

「場地屬性是易燃、易碎，還有就是光線意外地暗。」

「哦、哦哦……」

春雪點頭之餘，低頭看了看自己的身體。不知不覺間，粉紅豬的外型已經換成了那纖細的銀色機器人模樣。

春雪想知道黑雪公主又會是什麼模樣，於是轉動視線一看，但站在他眼前的，卻是和先前如出一轍的漆黑禮服虛擬角色。

「這就是你的對戰虛擬角色（Duel Avatar）嗎?『Silver Crow』，這名字挺不錯的嘛，顏色也很漂亮，線條我也挺喜歡的。」

黑雪公主的手伸了過來，摸了摸滑溜的銀色頭部。

這確切的觸感，讓春雪再次意識到這個空間是真正的虛擬實境，沒有什麼「禁止接觸」這類幼稚的倫理規範。

「謝、謝謝學姐誇獎……我倒是覺得挺像個小咖的。這應該不能……重新設計吧?這身造型和角色命名到底是誰弄出來的?而且什麼叫做對戰虛擬角色?」

「就是字面上的意思，專門用來對戰的虛擬角色。設計者是BRAIN BURST程序，同時也是你自己——你昨天應該做了個非常漫長、非常可怕的夢吧?」

「是啊……」

雖然想不起夢境為何，但他隱約記得那是一場非常可怕的惡夢，忍不住用堅硬的手掌搓了搓纖細的機器人手臂。

「這是因為程序進入了你的深層心象。BRAIN BURST會切開程序擁有者的慾望、恐懼跟強迫症觀念，濾除雜質，塑造出專用的對戰虛擬角色。」

「我的……心象，恐懼跟……慾望。」

春雪自言自語地說了這幾個字，再次低頭看看自己的身體。

「這就是……這副又小又脆弱，看起來還滑溜溜的身體，就是我自己想要的?當然我是一直希望可以更瘦一點啦……只是就算這麼說，我還是希望可以更有點英雄的樣子……」

「哈哈哈，事情沒有這麼單純。程序讀出的不是理想中的模樣，而是人的自卑感。以你的情形來說，光是沒有直接拿那只粉紅豬來當你的對戰虛擬角色，你就應該要感到慶幸了。當然我也挺喜歡那種造型就是了。」

「請……請妳別再說了，我可討厭得很。」

春雪心想：得趕快重新組一隻新的黑騎士來當校內網絡用的虛擬角色。接著開口問道：

「可是，這也就是說學姐妳現在這款校內虛擬角色，也是BRAIN BURST程序做出來的囉?這象徵學姐的自卑感?明明那麼漂亮……」

「不……」

黑雪公主眼神中微微出現陰影，低下頭答道：

「這是我自己用編輯程序拼出來的。我……有不可告人的苦衷，所以封印了原本的對戰虛擬角色不用。理由將來我會告訴你，時候到了我就會說。」

「封印……?」

「很遺憾的，我的對戰虛擬角色可是很醜陋的，而且醜陋到了極點。雖然我並非因為它醜陋才予以封印……不過我的事不重要。」

黑雪公主聳聳肩膀，馬上又恢復了一貫的神秘表情。她再次用白皙的手掌輕輕摸向春雪的安全帽頭。

「你今天早上透過全球網絡被其它超頻連線者找去對戰時，就用這個剛做好的虛擬角色應戰，然後輸得一塌糊塗。沒錯吧?」

「……嗚，是沒錯，而且還讓對方滿血打贏。」

「你應該有仔細看過對戰結束後的戰鬥結果畫面吧?」

春雪心不甘情不願地想起了上學前突然被丟進去的「對戰場地」。自己就是在那個陰暗的廢墟裡，被一名騎著沒品味的機車，戴著骷髏安全帽的騎士撞得飛來飛去，整條體力計量表轉眼之間就消失無踪。

隨著一陣沒出息的音效，眼前出現了一串文字顯示【YOU LOSE】，接著——

「我記得……有顯示我的名字、等級1，還有一個很奇怪的數字。是叫做BURST……POINT嗎?這個數字當時就從99減成了89。」

「很好，你已經記得很清楚了。BURST POINT!!就是它，就是這個點數驅使我們來到這個殘酷的戰場。」

黑雪公主用幾近大喊的聲音說道，隨後朝玻璃窗走了幾步，滴溜溜轉過身來，將拿在兩只手上的傘猛力朝地板上一插，撞得地板龜裂，石屑四散。

「BURST POINT就是超頻點數，說穿了就是我們可以『加速』的次數。每加速一次，點數就會減少l點。剛安裝後的起始值是100點，但是你昨天已經在交誼廳裡加速過一次，所以消耗了1點，然後剛剛你又用掉了1點。」

「不會吧……那、那這個點數要怎麼增加?該不會是要拿現實世界中的錢去買?」

「不對。」

黑雪公主斬釘截鐵地否定。

「增加超頻點數的方法只有一種，就是在對戰中獲勝。如果對戰雙方等級相同，只要贏了就可以增加10點：但是輸的一方則會減少10點，就像你今天早上的遭遇一樣。」

黑雪公主將臉轉向窗外滿天晚霞的天空，自言自語般的接著說下去：

「『加速』是一種非常強大的力量，不但能輕易讓人打架打贏，甚至要在考試中拿到滿分、在部分賭博或運動項目中獲得全勝，都是易如反掌。今年夏天的甲子園賽事裡，刷新全壘打大賽記錄的一年級選手，就是一個高等級的超頻連線者。」

「這……」

看到春雪聽得啞口無言，黑雪公主用悲傷的視線朝他瞥了一眼。

「而我們既然嘗過這種能力的甜頭，就只能想辦法永遠不斷『加速』下去。也因此，為了獲得這個超頻點數，我們只能永無止境地對戰下去。」

「……等……等一下。」

咦咦?妳說那個天才強打者是超頻連線者?

不對，重點不在這裡——黑雪公主的話裡似乎有矛盾?

春雪拚命思索過之後開口問道：

「請……請問一下，剛剛妳說只要贏得對戰就可以增加10點，輸了就會減少10點對吧?這……除了對戰以外，使用『加速』也會消耗點數，那麼所有超頻連線者所擁有的點數總和不就只會減少，不會增加?也就是說，不擅長對戰的人當然也就有可能會讓點數降到零……萬一真的降到零，會有什麼下場……?」

「你果然不簡單，吸收得很快。答案很簡單，就是會失去『BRAIN BURST』。」

黑雪公主的暗色眼眸浮現了燃燒般的色彩，筆直望向春雪。

「程序會自動反安裝，此後再也不能重新安裝，就算換上新的神經連結機種也沒用，因為程序會根據每個人固有的腦波來辨識。任何人一旦被剝奪所有點數，就再也無法『加速』。」

她以寒冰般的嗓音這麼宣告之後，又補上了一句但是：

「當然也有像你這樣後來才參加的新人，所以整塊大餅並不是只減不增。不過就算是這樣，點數總和還是有緩慢減少的趨勢。」

然而春雪卻幾乎沒有把後面追加的這番話聽進去。

「失去……『BRAIN BURST』。」

自己明明只嘗過兩、三次「加速」的滋味，但光是想像失去之後的情形，就覺得背脊仿彿凍成冰塊。這不只是因為會再也無法加速，另外還有一個原因，那就是一旦失去「加速」，春雪將會失去原本就跟他活在不同世界的黑雪公主之間唯一的接觸。

這時他總算體會到被那名骷髏騎士搶走的10點有多麼沉重了。

「好了……春雪，你要怎麼做?」

聽到這句耳語般的詢問，春雪抬起頭來響應：

「什麼……怎麼做……?」

「你現在要回頭還來得及。回去那個沒有『加速』，也沒有『對戰』的平凡世界。霸凌你的笨蛋也不會再出現了，這點我可以用學生會幹部的身份向你保證。」

「……我……我……」

——什麼加速還是BRAIN BURST都不重要，我只是不想跟妳分開。

但春雪當然說不出這種的話，而是握緊了銀色的拳頭這麼回答：

「……我還欠學姐人情沒有還。」

「哦?」

「妳給了我BRAIN BURST，把我從那個地獄裡拉了出來，妳這麼做不是為了搶走我預設的l00點，這點我還看得出來。畢竟如果妳是為了點數，話總是有辦法說得更漂亮的……既然不是為了點數，那妳應該有事情想要我代勞。這個目的對妳來說應該很重要，所以妳才會那麼大費周章，去查我打壁球的高分記錄，還從頭教會我加速是怎麼回事。不是嗎?」

「唔……你的推論很合理。」

春雪隔著銀色的面罩，正視著微微露出笑容的美麗虛擬角色。

「我……像我這種人，本來根本不可能跟學姐這樣說話。我不但長得丑，而且又胖又愛哭，還會對僅有的兩個朋友記恨、嫉妒，講不到兩句話就拔腿開溜，真的是糟糕到了極點，差勁透了。」

儘管他自己也覺得：我沒事提這些做什麼!但話卻像潰堤的洪水般停不下來。所幸虛擬角色的臉部像鏡子一樣沒有表情，算是不幸中的大幸。

「我明明這麼差勁，但是高高在上的黑雪公主卻肯跟我說話，還和我直連，哪怕明知那只是因為我玩遊戲的技術比別人好上一點，哪怕明知沒有其它理由，可、可是我還是不希望就這麼結束，就是說……」

自己真的是有夠不會講話，就不能先整理好想法再說出來嗎?唉唉，這種時候才正應該加速啊，啊、不對，現在就已經在加速了。

春雪陷入前所未有的恐慌狀態，但還是無法不吐露心聲：

「所以……所以我希望可以響應學姐的期待，我希望自己可以好好回報妳對我施捨的……慈、慈悲。雖然我不知道自己能做些什麼，可是如果妳現在有遇到困難，任何能幫上忙的事情我都希望去做。所以我……我不會反安裝BRAIN BURST。我要挺身而戰……以一個超頻連線者的身份挺身而戰。」

我在搞什麼鬼，只要講最後那句不就好了!我到底在扯什麼東西啊?

吐出所有心聲之後，春雪羞得把細小的虛擬角色縮得更小，更不敢抬起頭來。

他已做足心理準備，認為黑雪公主一定會覺得「這個會錯意的人，也未免太自以為是了吧」。沒想到下一刻，一句斷斷續續的說話聲音撼動了他的聽覺：

「不要用……慈悲這種說法。」

春雪心中一顫，以微微上抬視線捕捉到的，是一張這幾天來表露出最多情緒的臉孔。

「我只是個愚昧而無力的國中生，跟你站在同一個地方，呼吸一樣的空氣，一樣都只是個人。何況到了這個場地，你跟我就是完全對等的超頻連線者。刻意保持距離的人是你，虛擬世界裡的區區兩公尺，對你來說就真的如此遙不可及?」

話一說完，她就無聲無息地伸出了白嫩的右手。

就是這麼遙不可及。

春雪在心中這麼自言自語。

妳不了解對於我這種人來說，光是走進像妳這樣擁有一切者的視線範圍，就是件多麼令人戒慎恐懼的事情。我只要當個僕人就好，只要能當個忠心聽妳命令行事的棋子，就已經幸福得喜出望外了。要是現在反握這只手，我就會產生不必要的期待，產生一種日後一定會化為加倍後悔回到自己身上的有毒期待。

對千百合跟拓武也是一樣。我只要當個對他們兩人來說是個挺逗趣的胖子朋友，就已經心滿意足了。自己明明只求不被憐憫或同情，並不奢望更好的定位。

春雪口中吐出的聲音，就像吹在虛擬夕陽下的焚風一樣乾燥。

「學姐把我從地獄裡救了出來……這對我來說……已經是一輩子的幸福了，我不會期望更多，絕對不會。」

「……是嗎?」

黑雪公主自言自語地放下了手。

一陣生硬而沉重的沉默，支配了整個場地好一會兒。而打破這陣沉默的，是她那聽起來跟往常沒有絲毫兩樣的流暢話聲：

「你的志氣我就心懷感激地收下了。我現在的確得面對一個有點棘手的問題，希望藉助你的力量幫我解決。」

春雪小聲呼了口氣，點點頭回答：

「好的，只要我能力所及，什麼事我都肯做。那我該做什麼才好?」

「首先我要你學會怎麼『對戰』。體力計量表下方有顯示你自己的名字，你點選看看。打開『設定』選單，可以查看你的對戰虛擬角色上面所設定的普通招式跟必殺技等全身指令。」

「必……必殺技?」

春雪停住伸到一半的手，復誦著反問。

「嗯，程序在創造對戰虛擬角色時，會根據屬性來將固定量的潛力點數分配到各個能力參數上。有的類型強項是攻擊力，有的強項是防御力；也有些類型比較刁鑽，擅長靠必殺技一舉反敗為勝。可是以大原則來說，只要對戰虛擬角色的等級相同，總潛力點數都是完全一樣的。你雖然在第一次對戰中慘敗，但那並不是因為對手比你強，只是當時你還不懂得戰法而已。」

原來那個機車騎士「Ash Roller」跟春雪一樣是1級。當時春雪只覺得這個對手壓倒性地比自己要強，原來他的戰鬥力其實只跟「Silver Crow」差不多?

如果真是這樣，那麼這具瘦小的機器人虛擬角色上，肯定設定了威力非常強大的必殺技。春雪滿懷期待地伸出銀色手指，按下自己的名字。

一個半透明的窗口隨著音效開啟。

窗口裡以簡單的人形動畫來顯示身體動作，並在右方顯示招式名稱。

首先是沉腰握起右拳往前擊出的動作。普通招式「拳擊」。

第二招則是右腳後拉往前踢出的動作。普通招式「踢擊」。

最後則是必殺技——雙手交叉於胸前，接著往左右張開，猛力將頭往前撞。招式名稱就叫做「頭錘」。

就只有這樣，除此之外什麼都沒有。

「……請問一下。」

春雪茫然地問道：

「上面只有普通招式的拳擊跟踢擊……還有必殺技也只是普通的頭錘而已。」

「哦?」

聽到這個情形，黑雪公主右手的手指抵著耳邊，側著頭。她看起來表情沒有改變，但春雪卻不敢再看下去，便馬上低下了頭。光是想像到她黑色的眼睛中會浮現出多麼失望的神色，就覺得全身發燙。

但他還來不及意識到這點，嘴巴就先動了起來：

「沒有，沒什麼，因為其實我早就隱約料到了。畢竟這個虛擬角色從外觀上就一副沒用的模樣啊，對不起，我辜負了妳的期待。沒關係的，妳不用再理我了，就請當作抽到空簽吧。」

「你這個……大笨蛋!」

春雪全身一顫，抬起頭來。不知不覺間黑雪公主已經走近到自己眼前，柳眉倒豎，以烈火般的雙眸低頭看著自己。

「你的人生要怎麼活，我不會去干涉，畢竟我也一樣只是個國中生。可是在BRAIN BURST，我可是資歷比你多出六年以上的先進。我應該說過所有對戰虛擬角色的潛力都是相等的，你這麼快就忘了?」

「可……可是，實際可以用的招式就只有拳擊、踢擊跟頭錘……」

「那就表示一定另有足以彌補的強處。」

黑雪公主稍微放緩了視線的壓力，以開導般的語氣說下去：

「這款對戰虛擬角色是你的心造出來的，怎麼可以連你自己都不相信呢?」

我最不敢相信的人就是我自己啊。

儘管心中這麼說，但春雪還是點了點頭。

「真是對不起，我會相信的……先不管我相不相信自己，至少妳說的話我一定會相信。」

聽到他這麼說，黑雪公主臉上微微綻放出笑容——儘管那只是苦笑。春雪也稍微放鬆了肩膀的力道。

「看來在學戰法以前，你還另外有東西得先學好才行啊。所謂的頑強……」

她微微一頓，一絲苦笑中帶著些微哀切的神色。

「所謂的頑強，絕非單指最後獲得勝利。我自己就花了太多時間才體會到這一點。而等到我體會到時，一切都已經太遲了。」

春雪完全無法領悟這段說得平靜話語裡的真正含意。他歪著歪頭思索，想要加以反問，但黑雪公主卻不給他這個機會，當場轉過身去。

「嗯，時間快要到了。」

一看之下才發現本來多達一千八百秒的倒數時間，已經只剩下區區二十秒了。

「那麼下一堂課就來實習吧。」

「啥……咦……?這話是什麼……」

看到春雪聽得瞪大了眼睛，黑雪公主臉上露出剽悍的笑容。

「那還用說，當然是去討回你丟掉的10點啊。」

這句話剛說完，「對戰」就在顯示平手的戰果畫面下結束，同時「加速」也跟著解除。

才剛回到現實世界中的交誼廳，黑雪公主更不給春雪機會發言，就立刻拔開了直連專用的傳輸線。

「好了!我們開始用餐吧，有田，飯菜都要涼了。」

說完就滿臉笑容地從桌上拿起了一支小小的湯匙。無可奈何之下，春雪只好也跟著將手伸向放在自己面前的大盤咖哩飯。這盤咖哩飯從櫃台拿到這裡，體感時間已經過了三十分鐘以上，卻還熱騰騰地冒著煙，讓胃都縮了一下。

周圍各桌則跟昨天一樣，對春雪集中照射責備的視線，他本來覺得還不如整盤端去學生餐廳的角落吃，但卻抗拒不了飢餓。才剛狼吞虎咽地扒進三口飯，就聽到坐在同一桌的高年級生找黑雪公主說話，讓春雪一口飯噎在喉頭。

「公主，差不多可以告訴我們了吧?我們都好奇心得要死呢!我們該怎麼看待妳跟這位仁兄之間的關係呢?」

春雪猛然抬起視線，就看到發言者是那名昨天也出現的、有著輕柔秀髮的學生會幹部。記得她是二年級，擔任書記職位。

「唔。」

黑雪公主將湯匙放到焗飯旁邊，以優雅的動作端起茶杯，微微擺出思索的模樣。周圍的學生不約而同地靜了下來。

「說穿了，就是我向他表白，而他甩了我。」

尖叫聲跟驚叫聲充斥於整個世界。

春雪咬著湯匙，抱起整盤咖哩拔腿就跑。

「我……我說學姐啊!」

下午兩堂課都在眾人針刺般的視線中度過的春雪，從走向校門的黑雪公主右後方壓低了聲音抗議。

「妳在打什麼主意啊!這下我又會被人霸凌了!絕對錯不了!」

「你這宣言倒是說得鏗鏘有力啊。」

黑雪公主嘻嘻一笑，一臉風涼地說了下去：

「我明明只是陳述事實啊，而且你看起來也並非完全沒這個意思呢。」

說著說著就迅速在自己的虛擬桌面上操作，做出彈指的動作。緊接著就有檔案透過區域網路傳來，讓春雪的視野內出現了一個閃爍圖示。點選下去後，眼前就開啟了一張大尺寸圖片。

那是一張照片，拍到的是自己咖哩飯的湯匙含在嘴裡，還張大嘴巴的白痴樣。

看到這張照片，春雪立刻大喊：

「嗚嘎啊啊!」

接著迅速拉起檔案砸進垃圾桶。

「妳妳妳到底是什麼時候拍到這種視野獨家特寫的啊!動作快也該有個限度!」

「沒什麼，只是做個小小的紀念而已。」

就連現在這樣談話的當下，周圍仍不停朝春雪身上發射似乎真具有殺傷力的視線。明知為時已晚，春雪還是忍不住縮起肩膀，但黑雪公主的身影那麼纖瘦，終究不夠他躲。

「你應該抬頭挺胸。因為這間學校裡被我甩掉的男生固然很多，反過來的情形可就只有你一個。」

「我就是要說這個，我到底什麼時候甩過學姐啦!」

「你這麼說可真過分，害我聽得多受傷……啊，別說這些小事了。」

黑雪公主只用「這些小事」就輕輕帶過整個問題，以鄭重的表情小聲說：

「一走出校門，你的神經連結裝置就會連上全球網絡。只要有超頻連線者待在包含本校在內的『杉並第三戰區』內，任何人都可以強制你進行對戰。你要在被人找上之前就先加速，從對戰選單裡找出『Ash Roller』來向他挑戰。」

「咦……戰區?這就表示可以對戰的範圍是有限的?」

聽到春雪的疑問，黑雪公主微微點了點頭。

「那還用說?就算允許你找遠在東京另一端的人對戰，多半也是還沒碰到面就會耗完三十分鐘了……總有一天，你會踏進不限制人數的多人連線集團戰用戰場，不過那是等級超過4級以後的事了，現在你還是該先專心應付眼前的戰鬥。」

黑雪公主以銳利度略增的嗓音為整段課程作結：

「我話先說在前面，輸了可沒辦法立刻再挑一場，因為一天對同一個對象只能挑戰一次。我也會去觀戰，可是很遺憾的我不能出手幫你……你別一臉喪氣樣，只要照我寫在郵件裡的方法去打，至少不會打輸。」

「好……好的。」

春雪吞了吞口水點點頭，接著在腦內剪貼第六堂課上到一半時收到的純文字郵件內文。

「這次才是你真正的出道戰。祝好運了，『Silver Crow』。」

被她啪的一聲從背後一推，春雪就踏進了沙塵漫天飛舞的人行道上。
