１

雷肯醒來後，用水桶裡殘留的水把布弄濕，擦拭著身體。布髒了的話，就在旁邊別的水桶擰乾。重複四次後，最後用剩下的水洗臉。
因為沒有桌子之類的東西，就坐到了床上果腹。是昨天回程路上塞到〈收納〉裡的食物。

換了樸素的衣服後出了宿屋。
右手提著的行李袋很煩人，就走到陰影處收進〈收納〉裡。劍也收了進去。這樣就沒有行李了。
在城鎮中以這種無防備的姿態行走，心情不會有多好。更何況接下來要去的地方，有個擁有怪物般魔力的女人。不是女人。是有著女性外觀的什麼東西。

用這種無防備的姿態靠近那個什麼東西，感到的只有不安。

如果是在森林或迷宮中遭遇強大的敵人，就沒什麼問題。戰鬥就好。這邊比較強敵人就會死，又或者是反過來。總之會好好地決勝負。
但是，在這種城鎮裡，面對壓倒性強大的對象，不去攻擊或防禦而是是保持著不動，實在是讓全身都不舒服。

儘管如此，總之不從那個怪物般的什麼東西，學會魔法藥的作法可不行。


２

「呀，來了嗎。那麼，旅行去吧。」
「什麼？」
「你阿，想學習魔法藥的作法對吧？」
「沒錯。」
「那麼得先從如何採取作為原料的藥草開始教起才行。所幸的是，這座城鎮的四周生長著各種藥草。嘛，所以我才住在這裡。」
「原來如此。」
「所以快換上旅行用的裝扮吧。」
「現在？」
「就是現在。」

不想被得知〈收納〉的事。

所以一瞬間打算，假裝回到宿屋去取行李。
但是，放棄了。

想對希拉保密所有的能力是不可能的。
那麼，就優先隱藏住絶對不能暴露的能力，至於難以糊弄過去的，或是被知道了也無妨的能力，不去保密會比較好。

「在這裡換就好嗎。」
「啊啊，我會在玄關外等著。換好了就出來吧。」

雷肯換上了穿習慣的黑衣，再套上貴王熊的外套。
接著思考了一下。

（希拉說）（旅行用的裝扮）（那大概是）（在別人眼中是旅行用的裝扮）（這樣的意思）

把行李袋拿出來並放入適當的行李，再把劍跟劍鞘取出掛在腰邊。

「換好了。」
「那，出發吧。」
「門沒鎖。」
「門鎖上的話，傑利可出去買飯時不就很不方便嗎。」
「猿猴會⋯⋯猿猴的魔獸會，自己出去，買飯嗎。」
「有很中意的店家吶。」
「不鎖上的話，藥不會被偷走嗎？」
「根本沒有鎖喔。架子上的壺的話，有下了點機關。是拿不出去的。」
「種在庭院裡的藥草呢。那種柵欄，有心就能簡單地破壊掉。」
「我家種的盡是些很難處理的藥草喔。賣不出去的。而且毒草還比較多，想闖進去的話只會倒大楣。小偷跟附近的壊孩子們，都切身體會過了。順帶一提，傑利可會幫藥草澆水的。」
「猿猴會看著藥草嗎。」

───

兩人走到了東門。

只不過，希拉的穿著怎麼看都是一般服裝。
但是，雷肯也不打算問了。

「接下來呢，你阿。出城門的時候，就說明說要出去採取藥草二十天左右。然後，一出去就筆直前進，大概五千步的地方，左邊有個河床。在那個河床附近等著吧。」
「知道了。」

照做了。
在河床等了一會後，覺得有點閒，就把武器拿了出來保養。

之後，一位女冒険者來到了河床。
年輕貌美的女性。
身穿有點暴露的方便行動的服裝，腰上掛著一把細劍。

女性無視著雷肯靠近河川，用水洗了洗臉。
雷肯結束了武器的保養，站起來，從擦著臉的女性的後方搭話。

「真慢阿。然後呢，要去哪。」

女性一臉驚訝地轉過頭。

「等一下。你，認得出我？」
「不是才剛分開嗎。癡呆了嗎，希拉。」
「不不，外觀跟剛才完全不一樣才對。」
「最初見面的時候，我看到的就是那個外觀。」
「你，難道說持有魔眼？」
「模演？」
「不，待會再說吧。跟上來。」

希拉跑了起來。最初的速度很普通，但進到森林中不用擔心被人看到後，就不斷地加速，最終的速度快得可怕。雷肯原本很輕鬆地跟著。但是一直跑著也不休息，疲勞慢慢累積，最後也沒辦法好好跑了。午後雖然休息了一次，雷肯也沒能消除疲勞。只能拚死緊跟毫不留情地奔跑的希拉。當體力要到極限時，希拉終於停下來了。