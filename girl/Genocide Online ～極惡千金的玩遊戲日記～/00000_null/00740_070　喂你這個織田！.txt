作者的話：
前情提要：義妹醬的胃呈音速壓縮著

來，大家一起來


════════════════════

「喂你這個織田！」
「咕噗⋯⋯！」

回家並登入後，我一看見織田就抓住了他的前襟。

「不麻利地把事情吐出來嗎？」
「甚、甚麼事⋯⋯還有現實名字是NG的⋯⋯」

說得對，在遊戲裡提及現實的情報是出局的⋯⋯要小心才行！但更重要的是！

「不准你拒絕給老娘與一條⋯⋯麗奈同學變得親密的機會！」
「語調完全不同啊⋯⋯⋯⋯啊，好的，怎樣都好啦」

為了吐槽怎樣都好，我要令他更加振作起來並且馬上認輸。⋯⋯⋯⋯這種膽小懦弱的樣子是不行的。

「不把全部都吐出來的話我就不給你看原稿啦！」
「說、說得也是呢────」

於是織田將事情的經過詳細地說出來了⋯⋯⋯⋯⋯⋯⋯


▼▼▼▼▼▼▼

「喂你這個織田！」
「咕噗，又來了⋯⋯⋯⋯」

這、這傢伙～！太令人羨慕了吧！！為甚麼一條同學會自願允許他同行啊！為甚麼在途中一直保護住他啊！通常是反過來的吧！⋯⋯不是這樣子啊這個混蛋─！！

「⋯⋯所以呢？」
「甚，甚麼？」
「所以與一條同學一起冒險有甚麼感想呢！」
「咕噗，真不講理⋯⋯」

這是甚麼傢伙！你透過遊戲跟班上的神秘美少女變得親密了？是那樣的輕小說嗎！！我也想當百合對象的候補啊？！！

「意外地她沒有防備，還經常將臉靠近過來⋯⋯⋯⋯」
「啊『啊』？？」
「咕誒⋯⋯！」

說啥⋯⋯？臉⋯⋯臉？那位太過美麗的一條同學將臉靠近過來⋯⋯？？糟糕，要吐了，我第一次產生對這傢伙的殺意了⋯⋯而且這傢伙明明發出了『咕誒』和『咕噗』的可悲的聲音，卻偷偷地在冷笑著⋯⋯！！死宅特有的優越感高高在上地滲出來了⋯⋯！！真「可」恨「！」！"

「⋯⋯而且在之前的活動裡，她用手繞著我的脖子並在我的耳邊向我竊竊私語了呢」
「咿─！」
「不、不要勒得這麼緊⋯⋯我會死的⋯⋯⋯⋯」

我也想在耳邊聽到竊竊私語啊⋯⋯我有信心我絕對會懷孕的，因為她是一條同學啊。

「我還要是她第一個朋友呢」
「⋯⋯嗯？！！」

說⋯⋯啥⋯⋯？！一條同學的第一個朋友是這傢伙⋯⋯？？

「喂你這個織田！！」
「咕、咕噗⋯⋯」

啊，現在這傢伙也裝成發出痛苦聲音的樣子在笑著吧！！這傢伙絕對是沉浸在優越感之中！！真「可」恨"～"！！

「還說了因為是第一次交到朋友所以請好好地教導她」
「啊『啊』？！」
「請我從此以後也多多關照」
「啊『啊』？！」
「不閉嘴的話就會受傷」
「啊『啊』？！」
「不過本性是溫柔的呢」
「啊『啊』⋯⋯⋯⋯⋯⋯⋯⋯啊？」
「嗯？」

本性溫柔？一條同學嗎？

「啥？一條同學才不溫柔吧，織田君？腦袋沒事嗎？」
「這、這傢伙！？」

因為真的是這樣的⋯⋯一條同學不可能是溫柔的啊！真是困擾，這種錯誤的知識居然傳播開去了。

「會好好地關心我所以很溫柔哦！雖然的確她玩遊戲的方式正如屠殺者這個稱呼⋯⋯⋯⋯」
「嗯？可是與遊戲沒有關係哦？」
「啥？那樣的話就更加是溫柔了吧？」
「哎～⋯⋯⋯⋯誤解辛苦了，我要拒絕同擔」
「這、這傢伙！？」

哎呀～受不了呢！雖然像我這樣的老手跟⋯⋯⋯⋯守護者總算能夠辨識，但新手太新了所以不會明白呢～，真是的─！好辛—苦─，真的好辛～苦─！

「⋯⋯有沒有能好好地交談的同志啊」
「這、這傢伙！？」

哎～，一條同學才不溫柔呢⋯⋯那只是『對周圍沒有興趣』和『仔細觀察四周』沒有矛盾地並存而已，所以說這傢伙就是外行啊～！

「嘛，嘛，雖然或許不單純只是溫柔而已⋯⋯」
「哎～⋯⋯」
「真、真的啦！即使搞錯了也好，我相信麗奈的本性是善良的人，我是這樣相信的！」
「啊，是嗎⋯⋯⋯⋯說起來織田第一次遇見一條同學的時候，一條同學已經跟其他人組成隊伍了吧？」
「！？」
「⋯⋯⋯⋯儘管是第一個朋友，卻不是第一個好友呢？」
「甚⋯⋯麼⋯⋯？！」

因為那個時候早已跟其他的誰組成隊伍將地下城第一次攻略了⋯⋯織田UCCU！哈哈哈！⋯⋯⋯⋯⋯⋯哎～，我也想在一開始組成隊伍呢～？

「但，但你甚至連好友都不是！」
「呼呼，真是笨蛋啊織田君。太笨了⋯⋯！真的太笨了！」
「這、這傢伙！？」

我也知道這種事⋯⋯可是呢？！

「我會將一條同學的第二次、以及第一位同性的朋友＆好友登錄拿到手的！！」
「你、你在說甚麼─？！」
「作為異性的你是做不到的！而且我還有在長年的跟Zong⋯⋯⋯⋯護衛任務裡培養出來的，對這位一條同學的洞察力啊！！沒有任何理由會無法變得親密呢」
「你剛剛說了你是跟蹤狂吧⋯⋯⋯⋯咕噗？！」
「織田君，你一直都在說多餘的話呢？」

確實在活動時應該也是對一條同學說了多餘的話才被攻擊的⋯⋯⋯

「你真的是微妙地令人遺憾的傢伙呢」
「不要說微妙地令人遺憾」
「哦？要頂嘴嗎？今次冬CM的同伴是森田嗎？垰嗎？還是說兩人都是？」
「非常抱歉，我會死的」

秒速轉回來了呢！果然是一如既往的織田，我放心了。

「好⋯⋯⋯⋯下次會來你家玩的，還請多多指教哦？」
「⋯⋯期限還不要緊嗎？」
「不，這次暫且完全沒有問題呢？因為想要有與一條同學變得親密的時間」

雖然我未被逼到那種緊急關頭⋯⋯不過無論我有多少時間，這次我都要絞盡勇氣踏出與一條同學變得親密的一步。

「⋯⋯嘛，好吧」
「非常感謝你的合作！這才是我的織田啊！還會讓你第一個看原稿的！」
「好好」

哎呀～，果然需要的就是宅友呢！變成修羅場也能幫忙呢～。

「不過但是你搶先的事是無法抵消的！」
「⋯⋯給你牛〇妹的臉頰包」

（註：指商品「ペコちゃんのほっぺ」）

「原諒你了」
「真容易搞定（小聲）」

甚麼嘛，織田意外地是明白事理的傢伙啊，果然爭吵是不好的，對話才是人類領先於其他動物的睿智啊。

「那我也差不多────」
「哦，優居然在這種地方嗎」
「咕啊Ｗ耶Drftgy噗咿這Lp」
「⋯⋯⋯⋯這是，怎麼了？」
「請不要在意，只不過是超出了極限的御宅」

本人冷不防地登場令我的心臟臨近過勞死了，勞動基準法何在呢⋯⋯⋯

「啊，剛剛好呢。麗奈，這位是────」
「嗯？！織田，你⋯⋯⋯⋯」

抱歉呢，見面後馬上就勒住了你的脖子⋯⋯果然你是我的心之友────

「似乎就是跟蹤你的跟蹤狂────噗？！」
「喂你這個織田！？」

你都將甚麼曝光了！？想被沉在東京灣嗎，啊啊？！

「記住了？！我不是跟蹤狂而是守護者！不要搞錯了！」
「咕噗、⋯⋯噗啊」
「⋯⋯⋯⋯究竟，是怎樣一回事呢？」

注意到憧憬的一條同學冷眼望著我們的醜態後，我通過了對這個織田【叛徒】的制裁決議案。


▼▼▼▼▼▼▼

作者的話：
喂你這個織田！

嗯？聖誕節・前夕？⋯⋯（『ω』）抱歉不是很懂你在說啥

作者：寫完新作的序章了～！
熟人：哦—

作者：我每天必定會寫一話Genocide Online哦？很了不起吧？
熟人：即使是世界上在聖誕節的時候⋯⋯嗚⋯⋯！！

作者：我要哭了