「呼呣，相當陰暗了吶。」
爺爺大人一邊仰望著天空，一邊嘟噥著。我以被爺爺大人抱著的狀態下，也一起仰望著天空。

「看不到太陽公公呢。」
一望無際的陰天。雖然王都的天空上還是白厚厚的雲朵，但是遠處卻烏雲密佈。最近天氣變暖後，空氣就開始變得潮濕了呢。

今天讓爺爺大人帶我去孤兒院玩，只是好像快要下雨了。也許不能在外面玩了呢。

「爺爺桑嘛，快點啦！」
「噢噢，是啊。走吧。」
和爺爺大人一起去孤兒院的時候，總是步行的呢。縱使和母親大人還有侍女們一起去的時候都是坐馬車的，然而爺爺大人喜歡走路。說是為了健康。因為我是被抱著走的，所以很輕鬆呢。

「您好庫加大人！歡迎光臨。喲！艾麗娜。」
凱薩爾君站在孤兒院的門旁邊，向爺爺大人和我打招呼。

「呀啊，凱薩爾今天也很精神啊！怎麼了？特意到門前迎接嗎？」
「嗨咿！很精神喲！咿、咿耶，只是湊巧而已。庫加大人。」
凱薩爾君他，最近總是在門前等著我們。不和大家一起玩嗎？來孤兒院的那天，總是會事先聯絡的，但卻並不會按時間來啊。是一直在等嗎？

「凱薩爾君，你好。最近總是，在門的旁邊呢？」
「欸？才、才沒有總是啊～。習慣，對！只是習慣了而已。」
「是嗎？可以，再一起玩嗎？」
「啊啊，當然可以啊。不過，得先去和院長老師打招呼呢。我會在大家總是去的房間裡等待的，趕快去吧！」
「嗯，等下見哦！」
爺爺大人看著凱薩爾君向孤兒院跑去時，就露出了比平時更加笑眯眯的臉後，便慢慢地走去院長室。當然，我是被爺爺大人抱著的狀態下，去向院長老師打招呼的喲。

話雖如此，為什麼凱薩爾君要說謊呢？你總是在門口站著的吧？肯定是這樣沒錯。鑑於同孤兒院的涅伊莉醬也曾說過「一到我們來慰問的時候，凱薩爾就坐立不安，還挺讓人鬱悶。」鬱悶是什麼意思呀？

還有，不可思議的是我們的視線不太會合上。凱薩爾君看著爺爺大人聊天的時候，是看著爺爺大人的眼睛聊天的，和我說話的時候視線就會稍微向下移。剛才我因為是被爺爺大人抱著的關係，所以我的臉也和爺爺大人一樣高。固然，他卻只盯著我的腳看。

生病的時候腳動不了，所以治好了的現在變得很奇怪了嗎？下次試著問問吧。

跟院長老師打招呼後，爺爺大人就會像往常一樣跟院長老師說一些聽不懂的事情，所以我就一個人來到了平時大家都在的房間。

「啊，艾麗娜醬，歡迎光臨！」
「艾麗娜醬，歡迎呀～」
涅伊莉醬搖著茶色辮子的頭髮，馬上就跑過來了。之後閔美醬也是，搖著長到肩膀的金髮跑過來。

我們手牽著手，一邊轉圈圈一邊蹦蹦跳。我們已經相當要好了喔。

就像姐姐一樣的涅伊莉醬她、非常的溫柔，總是照顧著我。藍色的眼睛也很漂亮，非常喜歡。同齡的小閔美也願意和遲鈍的我一起玩。在孤兒院裡很多的朋友中，和這兩人的關係是最要好的。

正在跳來跳去的時候，凱薩爾君和蓋爾君也

到這裡來了。這４個人是經常玩的朋友。但在房間裡，也加入了更加多的其他孩子們，不過，在這裡的５人像平時一樣不變的在一起玩。

「老師說，因為要下雨了，所以今天就留在房間裡待著喔。」
「誒～，這樣啊。那麼，玩過家家怎樣？」
凱薩爾君把老師的吩咐告訴了我們。果然要下雨了啊。房間裡也總覺得陰暗，很悶熱呢。

然後涅伊莉醬提議等下要玩的遊戲。

「嗯～過家家什麼的，很無聊吧？蓋爾。」
「是這樣嗎？玩過家家也很開心喔。」
凱薩爾君說過家家很無聊呢。蓋爾君好像覺得玩過家家也可以的樣子。上次玩的時候，涅伊莉醬是媽媽、蓋爾君是爸爸、閔美醬和我是姐妹、凱薩爾君是寵物角色。即使不知道該做什麼，固然很有趣。因為大大只的凱薩爾君是寵物，小小隻的蓋爾君卻是爸爸呢，所以會覺得怪有趣的。然而在凱薩爾君向我和閔美撒嬌時給他抱抱的時候，涅伊莉醬生氣了。那時候看起來真的很開心呢。由於那時裙子被涅伊莉醬扯著的狀態去抱抱的，所以差點摔倒呢。

「那麼，想玩什麼呢？」
「誒哆，也是啊—。天也黑了，可以在黑暗中也能玩的遊戲就好。」
這樣啊，確實太暗了就什麼也看不見了，於是想玩過家家也很難吧。凱薩爾君很聰明呢。

大家絞盡腦汁想著玩什麼呢的時候，比涅伊莉醬、凱薩爾君還年長的哥哥、姐姐們和其他的年少孩子們一起過來了。

「因為天變黑了，老師說把大家聚集起來一起玩呀！」
「所以，大家一起來講恐怖的故事吧? ！」

◤・・・・・・欸！？◢

「恐怖的故事？那是什麼？會很有趣嗎？」
我在向剛過來的，凱薩爾君的朋友們打聽。

「噢，說點可怕的故事，害怕的話就是輸了。故事說完了後，接著就是一個人一個人單獨的去洗手間然後回來的感覺喔。」
廁所在狹窄的走廊盡頭。白天也很暗，一個人去的話有點害怕。雖然現在是白天，不過窗戶多的這個房間也變得暗了，廁所那的走廊一定更加漆黑的吧。

之後大家都聚在一起，坐成圓形。由年長的哥哥和姐姐們開始的順序說起了故事。

「在黑暗的森林中，有個男人迷了路而一籌莫展。道路的外側周圍都是高大的樹，也不清楚方向。・・・・・・・・・・・・・・・・・・・・・・・・・殺手鯊就突然出現在那裡，向男人齜牙咧嘴・・・・・・・・・・・・・・・・。」
我把坐在旁邊的凱薩爾君的胳膊緊緊地抱住了。好、好可怕。殺手鯊好可怕。嗚嗚，討厭啊。不想聽這個故事。被殺手鯊襲擊時，又恐怖又痛又冷的恐懼不斷地浮現在腦海中。身體顫抖得直打哆嗦。無法停止顫抖。

「艾麗娜，很害怕嗎？」
凱薩爾君在我抱緊他胳膊的手上覆蓋上了他的手，然後小聲地問我。

「嗚嗚，嗚嗯，害怕。」
「這樣的故事都是胡言，不用怕。沒關係的。魔獸什麼的，不會來王都的哦。艾麗娜真是膽小鬼。」
「嗚嗚，可、可是···。」
「嚯啦，故事已經結束了，對吧？」
凱薩爾君，儘管很溫柔，固然貌似很遲鈍呢。可是，殺手鯊真的很可怕嘛。

魔獸的故事結束後，另一個哥哥又開始講起可怕的故事了。這次好像是關於死去的女人變成幽靈的故事。像使聲音顫動一樣的說話法式，讓故事變得更加可怕了喲。

◤・・・沒、沒關係，異世界的怪談盡是謊言。幽靈什麼的不可能真的存在。人死了就完了。靈魂這種東西一離開肉體就會馬上消失。要不然你當輪回轉生在吃灰嗎？・・・・・，不是有嗎。因為是異世界，所以也有的吧。反過來說，正因為是異世界才有幽靈嗎？欸？欸？真的嗎？有嗎？欸？幽靈嗎？騙人的吧？◢

總覺得，被我緊緊抱著的凱薩爾君也有點在顫抖的感覺。是因為我不停地顫抖的關係，所以傳染到了嗎？慢慢地抬起頭看向凱薩爾君，總覺得臉上有點蒼白呢。因為太暗了，所以看得不太清楚。凱薩爾君也在害怕嗎？

一個接一個的哥哥和姐姐們交換著說故事，以抖音的方式大聲講著可怕的故事。聽著的孩子們中幾乎都是害怕的孩子，不過，其中也有完全沒在意的孩子，固然也有在說著沒事逞強的孩子呢。

外面下起了雨。窗戶上落下雨珠。

「・・・・・・・・・・・那孩子夜裡，因為突然覺得有氣息而睜開眼睛，這時，躺在床上的那個孩子面前，出現了一個黑影般一樣的透明老婆婆。那個孩子想提高聲音尖叫，但是聲音發不出來，身體也完全動不了。眼前的老婆婆笑眯眯地用沙啞的聲音說著「我要把你殺了～」・・・・・・・」

◤嘻咕！欸欸？啊咧？總覺得有親身體驗過的感覺。好像有發生過和故事裡所說的一樣的事情呢。啊咧？不過想不起來呢？是什麼時候來著？是在受傷還不能動的時候吧。因為『私』還被封印著，所以才想不起來嗎？啊咧？◢

「凱、凱薩爾君、艾、艾麗娜我要、要回去了。嗚誒誒誒。」
我因為太害怕了，所以全身都緊緊地抱住了凱薩爾君。啊咧？只是總覺得，凱薩爾君好像也緊緊地抱住了我似的？

「不、不、不要緊。這樣的故事都是騙人的！一點也不覺得害怕喔！哈哈！」
凱薩爾君，真的嗎？不覺得害怕嗎？

最後的那位姐姐講的故事實在是太可怕了，大部分年少組的孩子們都在半哭的狀態。而我就是跳過半哭，連直接地真哭起來也跳過，變得雙眼空虛、茫然地流淚。因為太可怕了，就變得什麼也不想思考了啊。連身後也害怕會不會有什麼在而無法回頭了。後背好冷…。嗚誒誒——。

不知不覺間，外面的雨下得大起來了。房間裡也越來越暗。

「那麼，按順序的去廁所然後回來吧。年少組２～３人一起去也無所謂喔。初等部以上的要一個一個地去唷！」

◤不、不行了。廁所什、什麼的我不想去。我要和爺爺大人回家去～嚒，暫時不想來孤兒院了。我要回去～！！◢

「艾、艾麗娜！一個人去很害怕吧！我也陪妳一起去吧。好嗎？！可以吧？！」
「嗚誒誒。艾麗娜我，要回去～。要和爺爺大人一起回去～」
「庫加大人還在工作中吧。所以還不能回去啊。明白了嗎？要不我們一起迅速地去，然後馬上回到這裡，那樣就不用害怕了。」
「嗚嗚嗚嗚。・・・・・嗯，知道了，艾麗娜，會加油的。」
好可怕啊。好可怕啊。我想回去。誒誒。為什麼一定要做這種事啊？

雖然已經害怕得不得了，不過其他的孩子們好像都已經決定好結伴一起去的孩子了。這樣下去的話，我就必須自己一個人出發了，凱薩爾君要是能和我一起去的話，我會努力的。只是嗚嗚嗚嗚，討厭的事情就是討厭啊～。

========================================

菲利昂「久違的解說喲。」
莉潔「那、那是我的台詞吧！為什麼一臉高興地在解說啊你。」
菲利昂「艾麗娜會討厭鬼怪，是你的原因啊。正因爲你異常地討厭鬼怪，所以她才被影響了呢？」
莉潔「嗚咕！是、是啊。但我並不是討厭鬼怪本身，而是不擅長應付變成鬼怪的這種精神體或是怨念而已喲。」
菲利昂「咕嘻咕嘻。受此影響，艾麗娜也害怕起鬼怪本身。女神好可憐。」
莉潔「咕咕咕，反駁不了。」
