﻿
「所以說阿、他絕對是『誠醬』喔！」
「不、所以說他是誰啊？」

世渡愛梨對在勇者中也是讓人厭惡而顯眼的團體、在地球是女性總長的野島優佳、國際模特兒的清水乃亞、現代人卻是辣妹的天川瑠美的三人說了。

「為什麼會不知道！？他就是『誠醬』喔！？」
「怎麼可能會知道啊！？」

野島不斷的應付著世渡的不講理。

「愛梨都這樣說了、我們認識他嗎？」
「不？不認識喔？」
「那絕對不知道吧！」

雖然有清水的幫忙、但仍對不上世渡的話。

「話說、那個『誠醬』是怎麼了？他和愛梨是什麼關係？男朋友？」

對於天川的問題、世渡害羞的搖頭。

「不、不是啦......男女朋友什麼的～......不是讓人不好意思嗎～。......雖然並不是。」
「竟然不是喔！」
「哈......話題沒有進展啊。那麼？愛梨說那個人是誠醬、然後要怎麼辦？」
「咦？想要幫助他阿。」
「誠醬是在不得不幫助的狀況喔！？」

野島立刻吐槽了。

「話題完全對不上！我們對那個誠醬完全不瞭解、是要我們怎麼幫忙啊。」
「怎、怎麼這樣～......很無情耶～」
「我說的是正論耶！？」

無論世渡怎麼說著『誠醬』是個怎樣的人、不認識的野島她們是什麼也都不知道要怎麼辦。
更進一步的說、世渡對於和『誠醬』相會後也沒有考慮過接下來要做什麼。
但是、清水嘆息的同時也整理了一下現在的狀況。

「呼......總之現在知道的是、愛梨在找的『誠醬』、現在就在F班吧？」
「沒錯！」
「真麻煩、見了之後再考慮其他的事不就好了嗎？我實在不懂愛梨在考慮什麼。」
「突、突然去見面......不會被討厭嗎......？」
「真麻煩啊啊啊啊啊啊啊啊啊啊啊啊！」

野島理所當然地叫了出來。
清水安撫了這樣的野島、而天川突然開始笑了起來。

「我說啊、愛梨的反應啊～、不管怎麼看都是迷上了『誠醬』吧？」
「咦！？真的嗎！？」
「......嗯、我也是這麼想的。」

對於野島以外的二人說的、世渡他────。

「那那那那、那種事、沒......沒有喔！？」
『質疑（Doubt）』

三個人同時這樣說了。

「愛梨......太容易懂了吧......。」
「不、優佳看不出來......。」
「沒、沒有這種事喔？」
「算了算了。......總而言之、愛梨是喜歡那位『誠醬』吧。」

剛才還覺得麻煩的三個人、果然女孩子對於戀愛的話題立刻開始變得有興趣了。
對於突然眼睛開始閃閃發亮的三個人、愛梨表情抽蓄的說。

「要、要怎麼說呢......。」
「────快點老實的交待─！」
「嗚哇啊啊啊啊啊！」

被女高中生又或者是女初中生左右的人（揉）躪之後愛梨投降了

「我、我知道了啦！會說的啦！」
「很好。」
「沒錯沒錯、要懂得放棄喔？」
「喔、那麼就快點說！」

愛梨怨恨的瞪著三個人、嘆了口氣之後開始說了。

「哈阿......我和誠醬是在中學的時候認識的。雖然現在像這樣和優佳你們在一起、但以前都是一個人孤孤單單的喔？」
「真的嗎！？」
「愛梨嗎......？無法相信。」
「還真是意外～」

對於愛梨那衝撃性的事實、三個人都沒有隱藏驚訝。
這三個人是在愛梨進入高中之後才開始往來、從來都沒有說過國中時期的事。
變成像這樣，大概是在國中後半的時候吧。

「國中的前半真的是孤單的一個人......現在可以變成這樣、都是誠醬的關係喔。」
「咦？」

對於預想外的展開，三個人不由自主地發出了聲音。

「剛才也說了，只有一個人的時候、中午的時候都會到沒人的頂樓的樓梯吃午餐。還真是懷念啊。」

愛梨這樣說的時候、開始回想起了過去。

◆◇◆

「哈啊......。」

國中的時候。
我────世渡愛梨、一個人寂寞的嘆息著吃著午餐。
一個人吃著午餐......知道了嗎、那時我沒有任何一個朋友。是和現在不一樣的畏縮喔？
總而言之、我在進入國中後，在頂樓的樓梯發現了一個超棒的吃飯的地點、從那之後每天都到那個地方一個人吃便當。
我想不管在哪個國中都一樣、我的國中也是禁止到頂樓，所以才可以一個人吃飯。頂樓禁止進入的話、到頂樓的樓梯就沒關係。嘛啊、我想避難的時候還是會用到。
我也只是使用這裡、所以也沒有什麼不滿。
就這樣、一如往常吃完便當的時候────誠醬來了。

「咦？有人在喔！？」

對於突然出現的誠醬、我也僵硬在那裡了。沒想到除我以外，還會有來到這個孤單吃飯的地點的猛者。
對於誠醬的第一印象是......就是那個啊。典型的不讓人不想欺負的傢伙。
因為啊，超胖的，臉也不好看，讓人說不出來的外表。
還有獨特的臭味、雖然我不在意，但對其他人來說似乎是讓人討厭的味道。
因為這些原因、誠醬受到很厲害的欺負。
幸運的是、我因為沒有朋友所以也有受到欺負......。
總之、彼此都覺得這個地方不會有人、沉默持續了一陣子。
最先打破沉默的是......誠醬。

「那個......孤獨一人嗎？」
「才不想被你說！？」

意外之外的話、讓我想都沒想的吐槽了。
但是誠醬不在意我的反應，笑了出來。

「哈哈哈哈哈！確實、由我來說的話很奇怪啊！都想哭了！」
「對、對不起？」
「用疑問句的方式道歉會更加難過！」

這個時候誠醬開始緊張了起來、最一開始還一副受不了的樣子。

「那麼、為什麼在這個地方吃飯？」
「不......我想和你來這個地方的理由一樣......。」
「咦......所以真的是一個人吃飯！？騙人的吧！？」
「你不也是自己一個人吃還敢說我是騙人的......。」
「啊、說的也是。不、但是、我是因為被欺負所以才一個人吃飯、但你和我不一樣吧、也沒有被欺負的樣子。所以、為什麼你會在這個地方吃飯。」
「......和你沒有關係。」
「這麼說也是。但是、這個地方不能使用的話......就不知道其他可以一個人吃飯的地方了......。」

看了困擾的樣子的誠醬、我這樣說了。

「......那麼、要一起吃嗎？」
「咦？」
「你、不是正受到欺負？那麼、吃飯的時候不就要儘量放鬆嗎？」
「那真的是太感謝了......但你沒關係嗎？」
「這裡拒絕你的話，我不就像鬼一樣。」
「是嗎......那麼就照你說的。」

────這就是、我和誠醬奇妙的關係的開始。
從那之後我和誠醬中午的時候都會到頂樓的樓梯、孤獨的吃飯......不、和同樣孤獨的夥伴吃飯。
說話的時候、誠醬非常的緊張、雖然有時候一副受不了的樣子、但還是知道是個好孩子。
此外、愛好完全不同的兩個人、誠醬意外的是個非常好的聽眾、讓我覺得聊天很有趣。
這樣的日子持續了一段時間、我第一次交到朋友......不如說、『朋友』這樣的存在還是第一次遇到、讓我下定決心的說了。

「誠......誠醬！」
「咦？誠醬？」

誠醬一開始照著念了一次、誠醬發呆了一下並浮現傻笑的表情。

「對喔！因為是『柊誠一』的誠一、所以是誠醬！」

沒錯......誠醬的本名是、『柊誠一』。
因為從那之後一直叫著誠醬的關係、都快忘記了。
......而且、只有我這樣叫著『誠醬』......有種是誠醬特別的存在的感覺、覺得很高興。
總之、第一次被叫著『誠醬』關係，被嚇到了、誠醬立刻臉紅並笑了出來。

「那個啊、怎麼說呢......被這樣叫、覺得非常不好意思......。」

好可愛。
不、雖然不知道其他人怎麼想、但是、對我來說誠醬的笑容是史上最強最可愛的。知道嗎？對女孩子來說可愛就是正義喔？......不管是哪個世界也都一樣。
但是、我的要求不是只有這樣。

「然後、誠醬要叫我『愛琳』喔！」
「愛琳！？喂喂、彼此都是沒人要的？難度不會太高了......。」
「你說的好過分！？」
「開玩笑啦。但是......那個......那個稱呼方式......。」

誠醬嘴巴相當躊躇、一直不肯叫出『愛琳』、當我覺得沮喪的時候，誠醬慌張地叫了出來。

「愛、愛琳！」
「嘿嘿嘿～......真的很害羞啊～」
「我才是害羞到快死了！」

用兩手遮住臉、對那樣說的誠醬玩弄了他一會兒。
就這樣彼此換了新的稱呼、並且我們的關係變得更好了。
......沒錯。已經可以確認了......從這時候開始、我開始注意到誠醬是異性。
嗯、也沒有朋友的我、要突然將難度提高到戀人，我也不知道該怎麼做。
只是、沒見過中午以外的誠醬。
我這樣說了以後、誠醬迴避了......。
一開始是震驚、非常的痛苦......但是注意到誠醬是因為受到了欺負、不想將我捲入......即使如此、我還是覺得很痛苦。
然後突然有一天、一如往常兩個人中午吃飯的時候、誠醬用不可思議的表情問了。

「這麼說來、愛琳為什麼會自己一個人？」
「突然問了這麼過分的問題！？」

我這樣說了後、誠醬慌忙地繼續說了。

「啊、不、抱歉。沒有不好的意思。只是、愛琳和我不一樣、沒有受到欺負吧？所以......。」
「......我也是、不是喜歡才都自己一個人喔。但是、我非常不擅長和人說話......。」
「但是和我就可以這樣說話？」
「誠醬是......嗯......很容易說話。另外、我沒有自信......。」
「喂喂、更加有自信點！愛琳很可愛、再打扮一下、現在可以和我說話，只要可以和其他人接觸，絕對可以交到朋友！」
「可、可愛！？」

突然誠醬說著可愛、我不禁慌張起來。

「嗯？說了什麼奇怪的話嗎？」
「不、不是......因為突然說了可愛......。」
「......啊─......果然跟以前一樣沒有羞恥心的說個沒完啊但是、覺得好的東西不說出來的話是無法傳達給對方不是嗎？但是、我是真的這樣想的喔。」
「啊、那個......。」

誠醬說的話是認真的我是知道、所以更加的混亂了。
腦袋中一片空白、拚命地想著我要怎麼辦、但是注意到的時候嘴巴自然地動了。

「......誠醬......我打扮後的樣子......想看嗎？」
「嗯。」

誠醬沒有猶豫的點頭了。
────從那之後。我開始注意打扮了......。
最一開要怎麼做都不知道、看了各種雜誌來學習要怎麼化妝和選擇衣服、但只有自己一個人，這就是極限了。
所以、我第一次拿出勇氣和同班的女孩子說話了。
雖然很恐怖、但想讓誠醬看了覺得更可愛。
如果拿出勇氣說話的話......雖然自己也沒自信、但很順利地和一群女孩子熟識起來了。
此外、可以和朋友跟誠醬一樣的說話。
雖然注意到的時候我已經是辣妹（ギャル）風、打扮也沒問題了。
從那之後慢慢的我的學校生活變了。
首先、不只是女孩子也開始會和其他的男孩子說話。
最一開始和誠醬以外的男孩子說話很緊張、但這也隨著時間也變得沒有問題。
而且、雖然仍然不敢相信、但是受到男孩子的告白的次數增加了。
和任何人都可相處很好的男孩子、總是在女性間遊玩的男孩子......總之受到各種男孩子的告白......。
但是可惜的是、我一次也沒有心動過。
我不會選除了誠醬以外的人。
就這樣我孤獨的學校生活明顯變了......但是相對的、失去了最重要的東西。
午休的時候。
誠醬────沒有過來。
我持續在午休的時候到頂樓的樓梯、但是誠醬沒有回來了。
而且、因為有了新的朋友、所以可以自由活動的時間減少了。
所以中午的時候不會到頂樓的樓梯、而是和朋友一起吃飯。
這是第一次......我覺得孤單一個人也很好......是的、我沒有想到。
到最後、我在國中畢業前都沒有再見到誠醬。
幸運的是、高中聽到一樣的傳聞......或者說是、收到有人被欺負的情報了、儘管心情很複雜但還是覺得很開心。
但是、我還是不能見到誠醬。
也突擊過教室、也在學校內找過......還是不能見到。
就好像、我被避開的樣子......。
這是我之後才知道的、誠醬似乎是有一段時間都關在家裡。
那個內心堅強......總是表現積極的誠醬會關在家裡，實在是無法想像。
誠醬變成那樣、在我不知道的地方痛苦。
那個時候、幫不上忙的我......非常的後悔。
......誠醬支持了我。
那份感情我不想讓它沒有意義、我從我的立場......對於誠醬只有感謝，現在我過得很快樂。
就在那個時候、和大家一樣突然被帶到這個世界。
最一開始是非常混亂、但冷靜的想想、就是在這個時候我必須幫助誠醬......。
如果是現在的我、我認為我就可以保護誠醬────。

◆◇◆

「────嗯......感覺就像這樣......。」
「完全迷上了吧。」
「甜到都想吐了。」
「你也對那個誠醬那傢伙......辛苦了......。」
「很過分的感想啊！？而且優佳也代入太多感情了吧！」

瑠美和乃亞眼裡有點驚訝的樣子、而優佳聽了我說的話哭了起來。嗚、優佳就是這樣的人。

「雖然有很多想說的、但誠醬是我的恩人喔。所以、想對他說聲謝謝。然後、現在換我幫助他了。」

我這樣說的時後、三個人彼此看了一下。

「真是......真沒辦法啊！聽了這樣的話、也只能幫忙了！」
「說真的很麻煩啊、但如果是為了愛梨的話？」
「嗯。會幫忙喔。」

我聽了三個人的話、禁不住問了回去。

「真的可以嗎？但是大家和誠醬沒有什麼關係......。」
「沒關係。我們的愛梨被他照顧了。這樣的話我們也要感謝他。」
「我不是優佳你們的東西喔？」
「才不是這個意思！？快點給我察覺！」

優佳的吐槽、我忍不住笑了出來。
......誠醬。多虧從誠醬那裡收到的勇氣、我得到了重要的朋友喔。

「那麼、快點往F班突擊！」
「所、所以......這麼急著去見......不會被討厭嗎......？」
「真的很麻煩啊！？」

優佳再一次的吐槽了。


