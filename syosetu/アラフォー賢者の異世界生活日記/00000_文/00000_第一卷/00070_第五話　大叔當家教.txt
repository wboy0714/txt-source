早餐時很寧靜。

早餐會在規定時間送入房裡。沒在那之前醒過來，就會被強行從被窩挖起。順帶一提，早餐的味道很清淡，就算說客套話也並不美味，但也不算太難吃。他對難以形容的半吊子味道有點失望，但想到今天之後的事，大叔的頭就更痛了。

昨天，杰羅斯出於意外，拜見了瑟雷絲緹娜的裸體。

結果，激動得血壓飆高的爺爺衝來和丹迪斯一起拚命安撫了瑟雷絲緹娜。雖然熬過了這樣的試練，但問題是杰羅斯接下來要見那名受害者。

畢竟杰羅斯作為教魔法的家教而被僱用，還會停留在這座古城大約兩個月左右。老實說，沒什麼比這個還更尷尬。

每次想到這件事，他就會吐出不曉得是第幾回的嘆息。

「……話雖如此，現在這樣待著也沒用吧。我還是去瑟雷絲緹娜小姐的房間吧……唉。」

他的心情實在是很沉重。杰羅斯看了從她那裡借來的課本或資料，深切感到這世界的魔導士在魔法式知識上的程度有多低落。即使說網路遊戲時的玩家更先進也是可以。生產職業也能製作魔法，所以也可以把這世界的魔法研究視為停滯中。

「要教到什麼程度呢。至少確定是不能教她我們創造的魔法……真是太不妙了。不，因為沒有『賢者之石』，所以是無法製作的吧。教到初階積層型應該就可以了。」

杰羅斯使用的原創魔法耗魔效率佳，同時也很強力，但製作方法上有問題。

尤其就這個世界的水準，大範圍殲滅魔法的威力太凶猛了，假如被運用到戰爭上，應該會無差別地奪走眾多人命。

當中有許多難以處理的魔法，實在不是能教別人的東西。

通常魔法56音式，在製作威力強大的魔法時，需要大張的「魔法紙」，但杰羅斯他們「殲滅者」不一樣。他們不使用魔法紙，而會挪用製作魔法道具時會用到的最高素材──賢者之石。

他當初不知道是想到了什麼，不用56音的魔法文字，而使用了表示數字的10個魔法文字，並挪用機械語言，創出了新的魔法。

棘手的是，因為沒有東西可以寫入透過機械語言產出的龐大數字列，當時他才會利用可以記錄數個魔法式的賢者之石。為了大量生成這個賢者之石，他和伙伴一同展開了誇張到不行的戰鬥。他們在半路上拉進許多同志，費時三年六個月的時間制成的魔法，就是含「暗之審判」在內的數個禁術。

把魔法植入潛意識領域後，賢者之石就全部被用在武器或秘藥上了，目前杰羅斯不可能製作這些魔法。賢者之石需要許多超稀有素材，他不知道哪裡才有那些素材。進一步地說，想到要一個人製作這些魔法的時間，那真的是必須不斷進行誇張到不行的作業。

不管怎樣，製作殲滅魔法時根本就是地獄。要在魔法紙上以機械語言不停寫下魔法式，刻到賢者之石上，然後再啟動魔法，繼續進行修正作業。就像在同步進行不停反覆寫程式，與透過螢幕持續修正錯誤的除錯作業。

新魔法不抱著半開玩笑的心態去挑戰，應該也會以未完成告終。

製作魔法是段有苦有樂的時光，正因如此，才可能製作得出來。要獨自製作應該是件很荒謬的事情吧。就算有誰做得出來，強力的魔法也需要相符的等級。

新魔法除了一部分之外，難易度都高到沒有至少500級就無法行使。一般普遍認為，就算勉強使用得了也會一次耗盡魔力，威力上也幾乎不會有什麼效果。所以這部分應該不必特別擔心。

就他所見的世界法則，光是把魔法式烙在潛意識裡，是無法順利發動魔法的。如果沒有充分理解，偶爾也會以無法發動告終。因為那也關係到讓自己可能行使魔法的身體等級，與行使魔法需要的技能等級。可以使用的魔法，是取決於它們的相乘效果。

理解行使的魔法，並藉由相應的修練經歷實戰，並且受過訓練的人，才可以把魔法徹底運用自如。如果是包含瑟雷絲緹娜在內的那些魔導士等級，就算他們可以理解魔法，為了運用自如，依然需要勇猛的升級。新魔法的魔法式遲早會被解讀出來，或許會被某人做出來使用也不一定。但大叔只要不傳授魔法，現在誰也不會想去研究吧。

最重要的是，正因為那是非常費工夫的麻煩作業，他不打算再製作那種魔法。

上班族時代體驗過的截止前夕殘酷戰場，杰羅斯一點也不願想起。

杰羅斯一面思考，一面走在緊密鋪設的石磚地走廊。

他沒當過老師，所以在擔心自己能不能順利教瑟雷絲緹娜。

進一步的說，也因為昨天發生了意外事件，在這種狀況下不太能說是好的開始。

他不是對少女冒出了不規矩的想法，但對方會怎麼想就另當別論了。因此他才覺得郁悶。

雖然說要教魔法，但這世界的魔法是使用畫在魔法紙單面上的平面圖魔法陣。即便研究了他們的文明水準，杰羅斯的魔法也依然格外地優秀。

其實，過去發生過一場大戰，聽說在後來被稱作「邪神戰爭」的戰役上，高度魔法文明不著痕跡地從這個世界消失。其影響甚鉅，有關魔法的各種資料或文獻都消失了，文化水準才會顯著地低落。

現在的魔法是舊時代的模仿物，為了重現古魔法，他們將遺跡挖掘到的「魔法卷軸」或是舊時代的魔導書作為基礎，每天都進行著研究。

然而，目前似乎仍未出現重大成果。重現失落的智慧好像遲遲沒有進展。杰羅斯和瑟雷絲緹娜把歷史課本連同魔法課本（魔導書）一起借來，這些事實是他從上面得知的。

此外他也拜託了戴眼鏡的女僕，利用書庫借來的歷史書調查了大略的世界史，昨天晚上努力搜集這個世界的資訊到非常晚。畢竟他完全沒得到事前資訊，就突然被不負責地扔到異世界。他想在早期階段就獲得資訊也是很合理的吧。為了在這世界存活，資訊就會是不可或缺的。

「邪神的力量很驚人呢～會被逼到差點滅亡也是可以理解。話說回來，那個女神……祂好歹也做個售後服務嘛！連必備知識都沒有就把我丟到異世界……同樣轉生的人，就算出人命也不奇怪……」

從被邪神殺掉，同時引起導火線的立場看來，杰羅斯很擔心自己同鄉的安危。

然而他現在光要生存就已經竭盡全力，實在也沒有餘力替別人多做揣測。

想著這種事情的期間，杰羅斯抵達了瑟雷絲緹娜的房門前。雖然很猶豫，但還是提起勇氣敲了門。

◇　◇　◇　◇　◇　◇　◇

用完早餐的瑟雷絲緹娜，在自己的房間裡不停地扭動身子。

昨天的意外事件讓杰羅斯看見了裸體，她正因羞恥而害羞到忸忸怩怩。

再怎麼說，她都是正值敏感年紀的女孩。出生以來第一次直視成年男性的裸體，而被一股難以言喻的情感給困住。

『和爺爺不一樣……』

也不用刻意問「哪裡不一樣」。總之，她現在話好像很少。

昨天杰羅斯的裸體在她腦中揮之不去。

「大小姐……您再不冷靜下來，會給杰羅斯大人奇怪的印象喲。」

「可是，蜜絲卡～我還是很難為情。」

「就算大小姐這樣，杰羅斯大人也未必就會那麼想。從那個人看來，大小姐就像是個小孩。」

「是、是這樣子嗎？」

蜜絲卡的話讓瑟雷絲緹娜受到不小的打擊。

她的容姿確實是惹人怜愛的美少女，但杰羅斯的喜好是再成熟一些的女人。順道一提，如果是巨乳更好。所以目前就只把她當小孩看。

如果是十幾歲後半段，狀況就會不同了吧，但就大叔看來，現在的她體型依然很孩子氣。

「唉，大小姐對男性有興趣是很好，但像杰羅斯大人那種對象可不行。」

「為、為什麼呢？身為魔導士的才能就不用說，老師可是會自己開發魔法的有才能之人喲！再說……我覺得個性方面也無可挑剔。」

「就我所聽見的，杰羅斯大人乍看之下，大概是既敦厚又沉穩的人吧，但從相反的角度來看，他就會是個非常冷血的人。」

「冷血……？實在不像是那樣呢。」

她對蜜絲卡的話歪了歪頭。

杰羅斯在瑟雷絲緹娜的眼中很優秀，比任何人都強，有著善於照料他人的形象，她認為實在和冷血相距甚遠。可是，在蜜絲卡眼中好像看得見別的形象。

「您想想。他轉戰於戰場，不停地進行魔法實驗，對吧？換句話說，為了確認自己的魔法成果，不論是怎樣的犧牲或危險他都在所不惜。這表示他也擁有好于鋌而走險的危險層面。

對那位先生來說，戰場的戰士們或魔物都是很好的實驗材料。他是會毫不留情地殲滅，並確認魔法成果的喲。與其說冷血，不如說是冷酷……他也是那種就算是相當殘酷的事情，也會滿不在乎去做的人。」

聽她那麼一說，確實是那樣沒錯。瑟雷絲緹娜只看見表面形象。只要改變視角，杰羅斯就是在不斷重複著瘋狂行動。她被對方的優秀吸引目光，無法從不同的視點來看。比起這個，她一方面也是因為變得能夠使用魔法而歡欣不已。重新聽人這麼講，才發現到這件事。

「但，他說想要安靜過生活……」

「隨著年紀增長，也會有些想法吧。人不可能一直停留在同一個地方。」

「蜜絲卡，不知為何，總覺得你說的話非常有份量……」

「這就是經驗差距，大小姐。」

蜜絲卡的表情似乎蒙上了一層陰影。

「我偶爾在想，蜜絲卡，你現在幾歲呀？從我小時候起，你的外表就完全沒有改變，我以前就非常好奇了呢……」

「不能向女性詢問年齡喲。即便是同性也一樣……好嗎？」

蜜絲卡散發出危險氣息，讓人充分領悟那是不可詢問的事。

對她來說，年齡好像是禁忌。

──叩、叩。

『不好意思，我是杰羅斯，我可以進房間嗎？』

話題裡的人物隨著敲門聲出現。瑟雷絲緹娜就算不願意，心臟也像在敲響警報鐘一樣的失控。她再次想起昨夜的醜態。

「來、來了～～～～！」

「大小姐，請盡量冷靜下來，別表現得舉止可疑。」

「我、我會試著努力滴……」

「啊、吃螺絲了……」

多愁善感的少女，不小心再次想起剛才自己在煩惱什麼。

腦裡掠過了杰羅斯的裸體。

她第一天的課程，就這麼在愁苦的狀況下開始了。

◇　◇　◇　◇

杰羅斯走進房間，從借來的課本中抽出一本書，遞給瑟雷絲緹娜。

那是放在這棟別院書庫裡的古魔法相關書籍。相較之下，它寫了比較正經的魔法式研究或理論，記載著就課本來說很適合的基礎理論。

話雖如此，他認為裡面三成都只是與理論相距甚遠的塗鴉，但必要的就只有記錄著正確知識的頁數，所以他判斷沒什麼問題，決定把它當作課本。

「嗯，今天一開始的課程，我想從認識魔法文字開始。」

「魔法文字嗎？那被說是目前還無法解讀的神聖文字，老師您已經可以解讀了，對吧？」

「那出乎意料地單純喔。這是話語，同時也是回路，作為干涉、收集魔力的媒介應該也很方便。我想你馬上就學得會。」

「我知道這些事，但我真的學得來嗎？」

「只要理解就很簡單了。不過，要學到那種程度，各方面都會很麻煩就是了。」

魔法文字存在56個文字，與10個表示數字的文字。56字母也是一種記號，他們會將其排列成一個字來完成意思。雖然念法或發音語感不一樣，但它可以用日語解讀，只要了解意思就真的非常單純。偶爾會是英語、法語、西班牙語或德語……最後就連斯瓦希里語也有放進去，所以沒什麼比這還更麻煩。

儘管很多人在遊戲途中都發現了這件事，但因為那是由令人費解的話語所構成，因此杰羅斯記得解讀非常費工夫。

然而，若是這個世界的話，狀況就不一樣了。當地魔導士一點也無法理解這些話的意思。他們相信一個魔法文字裡就有意思，只把它們看作文字，因此才無法解讀魔法式。

文字有意義本身是沒錯，但他們在基礎上就碰到了瓶頸，所以才會沒辦法前進。

所謂的魔法式，就只是在製作含有引發物理現象意義的話語。該魔法式適用於魔法陣上，但如果不通曉基本物理現象，要做出魔法之類的就是不可能的。

「我、我都不知道呢。沒想到這會是為了構成話語的東西……」

「這好像被理解成是數字或記號。不過，它其實是古代使用的話語，單一個字是沒意義的呢。不過，因為魔力可以稍微發動，你們或許才會有所誤解。」

「老師，您是用這個文字製作魔法式的嗎？」

「是啊，雖然作法多少不一樣，但主要是使用把積層魔法陣效率化後的東西。不過，威力最大的魔法大部分都是數字的魔法文字呢。」

杰羅斯在原本的世界裡是程式工程師。

構築魔法是利用了積層型魔法陣的形式。不過，殲滅魔法另當別論。只利用0和1構成的魔法式需要非常大的勞力，和執行相當計算的處理術式。杰羅斯……也就是「大迫聰」，和伙伴與熟識的生產職業，以人海戰術進行了把魔法式刻入賢者之石的作業，並且構築了魔法式。「Sword and Sorcery」的玩家自由度很高，他們厭倦了一般的魔法製作，於是就和同樣有空的人一起用不同方式挑戰了製作魔法。

考慮到原本的職業，這就算被人說是作弊也沒辦法。雖然他本人不想再做第二次，但那其實並沒有什麼。

問題在於遊戲本身接受了這些新魔法。雖然說自由度很高，但它要處理的資訊量很龐大，弄個不好，遊戲本身也有出現bug的可能性。

『咦？……總覺得有股異樣感……好像有哪裡很奇怪，會是什麼呢？』

些微的掛心加速了杰羅斯的思緒。

身為主宰系統的前國防電腦管理系統，通稱「BABEL」。雖然它尚未完成，但擁有龐大的處理能力，判斷了在遊戲裡構築的壓縮程式──魔法式，是可以使用的。照理來講應該是這樣，但他第一次發現那本身是不可能的事。

『奇怪。「Sword and Sorcery」是由龐大程式構築成的才對。怎麼想都不可能接受新的魔法……魔法的製作程式也是一樣，就算要處理效果畫面，或日益增加的數據，持續長達七年的話，照理來說會變成一筆很恐怖的資訊量。

系統怎麼沒當掉呢？那很明顯超越了資訊的容許量……好奇怪，這是不可能的吧……』

就算說是使用了超級電腦，那也算是構築了一個世界，甚至NPC也擁有人類般的自由意志。即使只是AI思考慣例，資訊量也會極其龐大。就算壓縮了數據，那實在也不是可以處理的資訊量。

這時，他對於自己曾經享受的世界感受到強烈的異樣感。

「……只靠數字文字，那種事居然可行……好厲害。是說，老師，您有在聽嗎？」

「啥！不、不過，那很耗費工夫與時間呢。可以辦到的話，之後就輕鬆嘍。

要說有問題，就是會非常耗費製作時間，與非比尋常的勞力吧。你最好不要模仿喔。那可是地獄……」

差點迷失在思緒裡的大叔，急忙將意識拉回現實。現在可是上課中。

不過，他對「Sword and Sorcery」抱持的疑慮卻成了一個疙瘩，留在內心一隅。

「現在的我有辦法構築魔法嗎？說真的，我沒有自信。」

「現在是沒辦法，但不慌不忙地學習基本正確知識比較好。我想想……我們就試著分解『火炬』的魔法吧。因為沒什麼比基礎更重要。」

火屬性魔法的入門──「火炬」，只是點亮光明的魔法。

那是透過自身魔力作為燃料，再加上把外界魔力當成產生火焰的媒介使用，並進一步以調節空氣的魔法式去構築而成。

火沒有燃料和氧氣就不會引起化學反應，也不會有熱度。

例如，都市地區的柏油路，只要溫度持續過熱就會燒起來。如果是這種狀況，溫度就會變成火種，而柏油路就會成為燃料。溫度當然會持續上升，因此要控制就只有灑水一途。

要追求魔法效果，控制術式總是會被看得很重要。那幾乎沒什麼變化，而且也能使用在其他魔法上。只要可以理解控制術式的意義，剩下就只要把轉換物理現象的魔法式在知識上進行對照。

這天，瑟雷絲緹娜打開了新的一扇門。

兩小時後……

「就像這樣，所謂的魔法就是以機能為優先做出的東西呢。對術者的安全層面是非常重要的。如果自己因為自己的攻擊魔法而受傷，那就沒有意義了。許多創造出魔法的人都是費了心血，最終才總算抵達這裡。」

「意思是說，即使只是照明魔法也會耗費相應的時間和精力，對吧？」

「沒錯。只要可以解讀一定程度的術式，其他不明確的術式也可以靠語感去解讀。即使這樣也不懂的時候，也可以試著利用別國的語言。」

「像是精靈語、矮人語嗎？確實是有語言字典……原來如此，真有趣呀。」

「想成是話語的謎題就可以開心地解讀嘍。或許進展會有意想不到的順利。」

瑟雷絲緹娜懂了魔法式的事情。不過，這回則被其他事情吸引了注意力。

「可是，如果能用話語引起現象，做成這種魔法陣有什麼意義呢？假如只是要引起現象，我覺得就算只有魔法文字也足夠耶。」

「魔法陣是為了對魔法進行『將必要魔力作為現象構築』……換言之，那就像是蛋殻一樣的東西。在這個魔法陣裡收集必要的魔力，透過魔法式轉換成現象，並且把魔法發動前的工程統合在一起──你把它想成是這樣的階段就可以了。因為收集來的魔力擴散掉就沒意義了呢。」

考慮到發動效率的魔法，就算是小魔法也不會產生浪費，即使說它是門藝術也可以。

她看見魔法被分解的模樣，好奇心漸漸膨脹開來。

「話說回來，老師，您的魔法式是怎樣的內容呢？我非常感興趣。」

然而，那份好奇心不久也朝向了杰羅斯的魔法。

那同時會變成是要面對恐懼。

「我的……嗎？嗯……這個嘛……或許你先在這邊了解魔法的危險性也好吧。要是你用56音式魔法不小心創造出危險效果的魔法，也會很令人傷腦筋……」

「魔法的危險性……嗎？」

「鑽研到底的魔法，確實可以說是門藝術。不過呀，那同時也有將會奪取眾多生命，且極為凶猛的魔法……我的魔法就在凶猛魔法的最頂端，是極為危險的東西呢。」

杰羅斯邊這麼說，邊在手掌聚集魔力，現出魔法式。

那是裝了非常龐大的魔力，同時包含不祥與莊嚴兩種極端感覺的立方體。內部總是高速環繞極精緻且高密度的魔法文字，密度與入門魔法之間有著無法相比的壓倒性差異。這個發動的話，就會變成漆黑的球體。

雖然高密度的魔法式循環很藝術性，但同時也蘊含著背脊為之凍結的強大力量。就算不願意也可以從它釋放的魔力感受得到。

「…………這、這是……」

「這是我最強的魔法──『暗之審判』的魔法式喲。要是發動這個的話，這一帶就會瞬間消逝無踪。這就是魔導士的危險性。光是擁有強大的力量，就是很充分的威脅，從別國來看，應該會把這個當作無論如何都想得到的寶物吧。

一旦發動損害就會難以想像……」

「這、這……是怎樣的魔法呀！說會讓這一帶消逝無踪，這究竟……」

「這是大範圍殲滅魔法。以文獻上邪神使用的力量為基礎去調查法則，所製作出來的最高級破壞魔法之一。我半好玩做出來的結果就是這個。好奇心有時候也會不小心造出危險的東西。你最好先記住這點。」

少女對破壞魔法這一詞，露出了彷佛看見恐怖東西的驚愕表情。

文獻等等的都是胡說八道，但大致上是沒有錯的。

「為、為什麼要做出這麼強力的魔法呢？」

「當然是因為好像很有趣呢。你懂嗎？製作魔法確實很開心，但過分的好奇心會無意間造出這種危險物。傷腦筋的是，許多當權者都非常渴望這種魔法。也不去考慮它帶來的損害會有多嚴重呢。」

這個國家林立的世界上，破壞魔法是許多國家研究的東西之一。

如果擁有強力魔法，別的國家就不會攻打過來，但同樣也會助長侵略行為。其結果就是許多寶貴性命會被奪走，大地也會被蹂躪得體無完膚。

「在好奇心驅使下製作魔法是可以。我不否認那也會成為向上的原動力。不過你最好先別碰破壞魔法。那些結果帶來的只有悲劇，還會被死者的親人憎恨。

那份憎恨會進一步創造出類似的破壞魔法，泥沼般的無止境戰爭就會被延續下去。那根本就是受詛咒的連鎖。尤其是染上野心的當權者立刻就想使用。這就是魔法的危險性。我希望你把它想成這是絕對無法交給別人的禁忌。」

要弄清起因的話，魔導士的派系也是為了阻止這樣使用破壞魔法的安全裝置。

然而，派系間卻不知不覺開始競爭，各門派互相仇視並追求權力。同時為了獲得功績，而變得期盼戰爭。結果，其中也會出現私通他國、暗中活躍的人。大叔的主張就是──如果是會令人腐敗的理念，不要有反而還比較好。

「我想魔導士絕對不該擁權，要總是保持中立才行。而且除了破壞魔法之外，魔法應該也有其他用途。我認為魔法不是只有在戰場上才能有所發揮。應該也有許多其他事情可以做，所以追求那些事，應該也可以開拓可能性吧？」

「除了破壞之外嗎？例如，魔道具或魔法藥那種東西？」

「對……給別人帶來幸福的那種魔法，我認為有那種東西也很好。像是可以讓生活變富裕的那種，對吧？至少，你最好不要變成像我這樣。」

對瑟雷絲緹娜來說，既然生於公爵家，魔法就是在保護國家的戰爭上使用的工具。

她因為沒有保護人民的力量，所以受到冷淡對待。但她現在得到那份力量，肩上就會背起沉重的職責。可是，杰羅斯啟示了戰鬥之外的道路，不希望她和自己走向相同的道路。

瑟雷絲緹娜對此感到不知所措。

「瑟雷絲緹娜小姐，你想成為怎樣的魔導士？你有必須完成的目標嗎？」

「咦？我、我的目標……嗎？」

「魔導士只要戰鬥，就一定會有人死去。但是啊，魔導士不是只有戰鬥才是一切。我希望你好好思考，自己要以怎樣的魔導士為目標。針對那個目標，自己又可以多麼努力……」

「我、我……我不想當只為戰鬥的魔導士。但……」

「如果你的目標沒定下來，就先好好觀察自己的周遭吧。同樣也審視自己，只有一直不斷思考自己的存在方式，答案才會出來。

我可以教你魔法，但無法指示你明確的道路。我不是可以說出那種了不起的話的立場。因為我就是做出危險物的罪魁禍首……」

這終歸是遊戲的東西……況且也是神賜予的借力。

他沒有傲慢到會沉醉在那種力量。重要的是面對這份力量，杰羅斯還覺得太過危險，自己無法勝任。那不是可以任意使用的力量。雖然他有時常常忘記……

不過，就瑟雷絲緹娜看來，她覺得杰羅斯根本就是魔導士的理想型。

杰羅斯不迷戀力量、不與他人為伍的生存方式非常中立。雖然會教人魔法，卻沒意思把關鍵的研究成果轉手給人，這態度非常高潔。他不僅正面面對自己的力量，對那份危險也總是以嚴格的態度背負著責任。

事到如今誤會還是產生了。瑟雷絲緹娜變得更醉心於羅杰斯。

大叔明明只是在想：要是她不被破壞魔法吸引就好了……

「那麼，講座差不多就先到這邊吧。」

「是呀。我在這之後也有東西要學，必須做準備。」

「明天就在實技上消耗魔法吧。我會製造簡單的魔像，就用那個當作標的升級吧。」

「魔、魔像嗎？」

「對。打倒魔導士做的魔像也會有經驗值，所以要分類的話，應該會像人造魔物吧？不過，那東西比較弱，你可以放心地放手破壞。」

「那就麻煩您了，老師！」

瑟雷絲緹娜開始走上魔導士之路。

雖然現在還不曉得結果將會如何，但她看見魔導士的理想，以最優秀的魔導士這個頂點為目標，並且邁出了步伐。跟隨大賢者杰羅斯的身影……

那位大賢者對於「Sword and Sorcery」這款遊戲，開始有種至今都沒感受過的疑問。要明確弄清他抱持的疑問，則還需要一些時間。

◇　◇　◇　◇　◇　◇　◇

對瑟雷絲緹娜來說，那天的課程內容很充實。

有魔法式的解讀方法，甚至有更勝於此的高密度魔法式。最令她省思的，就是自己身為魔導士的樣子。

她迄今都用不了魔法，只是懵懂地想要變得能夠使用。

那絕不是徒勞的努力。就結果來說，她在伊斯特魯魔法學院取得了頂尖的成績。然而，無法使用魔法的瑟雷絲緹娜，同時也被貼上了吊車尾的標簽，落得受周圍侮蔑與嘲笑謾罵的窘境。

在她即使如此也不放棄，並開始要調查魔法式時，就遇見了杰羅斯。

杰羅斯不僅解決瑟雷絲緹娜的問題，也讓她看見未知的世界，還提出了其危險性。

學院裡絕對不會教那種事，不管對什麼都只以威力優先，完全沒有講師願意教面對魔法的心態。況且，他們連思考自己要以怎樣的魔導士為目標的時間都不會給，每天就是施法並審查其威力。

那裡不會有閑工夫思考未來。反過來說，學院每天就是反覆上單調的課程，也完全沒有嘗試讓學生成長。只要符合一定滿足條件，就算是不完全的魔法式，也要叫學生發動。不滿足條件的，就會被當作吊車尾捨棄。確實每天持續使用魔法，也會增加所保有的魔力吧，但除此之外不會有任何優點──她回想起這般被放著不管的情況。

身為教育者，他們的授課方向完全不行，還在學院裡派系鬥爭，根本就處理不了教學。派系不同的學生就冷淡對待，同門的話則予以優待。

這麼一來，如果有權力的話，他們就會變得更加偏心。瑟雷絲緹娜還沒被學院逐出，不外就是因為各派系追求索利斯提亞大公爵家的權力。

目前兩兄弟隸屬各別的派系，聽說現在成了兩大派系的頂尖旗幟。搞不好還會演變成接班人的爭奪。是領地裡可能引發內戰的狀況。

要是這種程度就可以解決還算簡單。雖然說是旁系，但那也是王族的血統。因為也會有王位繼承權，所以其選拔極為激烈，如果弄錯了一步，應該就會賭上這個國家的王位繼承權，並且發展成內戰。

外面諸多國家不可能會漏看這個破綻。那種學院裡充斥著自私的傢伙在宣揚權力，她認為這實在是很狹隘且可悲。

「學院的講師們，要是都像老師這樣就好了……」

對於懂事時開始，就不斷看見醜陋之事的她來說，她不覺得學院是那麼重要的場所。畢竟他們推薦有缺陷的魔法式，任職的老師們也有許多就連那項缺陷都沒發現。

與杰羅斯相比，老師們的層級遠低於他。最要緊的是他們無法讓自己能使用魔法，因此她無法把老師們視為尊敬對象。她對那些人在搞權力鬥爭的派系本身感受不到魅力。

想到兩個月後要回到那種地方，她的心情就愈來愈低落。

「哦！緹娜！你現在課上完了嗎？」

「爺爺！是的，課實在是既易懂又開心。」

「那就太好了。老夫可以問他教了怎樣的事嗎？」

「好的。離舞蹈課還有一段時間，如果是一下子，目前是有空檔。」

「嗯……隱居的我也只有這個樂趣了呢。」

對克雷斯頓來說，與孫女談天是最棒的娛樂。雖然總覺得他有點溺愛過頭……

兩人接著交談，起初克雷斯頓對於孫女的話露出喜悅的神色，卻在某時開始轉為嚴厲的表情。那是聽見大範圍殲滅魔法「暗之審判」之後的事。

瑟雷絲緹娜對於爺爺這樣的反應並不知情，繼續開心地說著上課的內容。

「嗯……魔法式的解讀方法嗎。緹娜啊……那件事情，你絕不能告訴別人。尤其是對派系裡的那些傢伙。」

「我明白。要是被人知道感覺一定不會是件好事。」

「嗯……可是大範圍殲滅魔法……仿效邪神的力量嗎，真可怕啊。」

「是的……老實說，我也變得很害怕。老師背負著那種危險東西呢。」

「他就是知道自己的危險性，才不希望拋頭露面的吧。」

克雷斯頓正在衡量杰羅斯瘋狂研究的恐怖程度，與同時身為教師的態度。

身為國家的重要人士，將這樣的人放任不管會很危險。他必須找機會靠關係維繫住才行，但弄不好又有可能會與他為敵。

另一方面，他作為講師是很優秀，不僅著眼孫女的未來，說明了魔法的危險性，還表現得讓孫女思考要以怎樣的魔導士為目標的態度。

雖然是一部分流派，但有些人會主張好戰的方針，說魔導士是以戰鬥為前提，像是此外沒其他道路似的切斷其他選擇。

不過，杰羅斯斷言「有讓人富裕的魔法也很好」，而且提到要探索有助於人民生活的魔法。

有鑑於這點，要是勉強讓他從軍，就會被當作是敵對的意思。貿然刺激他，他應該就會馬上隱藏踪影了吧。這怎麼說都是損失一名優秀的魔導士，但同時，若是戰事之外的事情，他帶給人可以請他充分協助的印象。

索利斯提亞魔法王國無論在名聲或實際上都有軍事國家的傾向，胡來的舉止可能會牽涉到存亡問題。所以他們無論如何都會希望接班人是優秀的魔法士。

況且，他們從來都沒想過，像是那種為人民貢獻力量的魔導士，所以有種恍然大悟的心情。

「有助於人民的魔導士……不需要權力嗎？如果只是那樣，實在也太籠統了呢。」

「可是，如果存在那種魔導士，我想就可以讓這個國家的人民欣然接受了。即使是魔道具也是一樣，我想重點應該是魔法的使用方式。」

「嗯……實際情況是有很多魔導士對人民態度傲慢，並受到責難呢。」

魔導士和傲慢的貴族一樣惹人厭。他們偶爾也會受到國家的嚴厲勸戒，但就算責備，在他們身旁的貴族們也會擱置這件事。某種意義上，這種行為也可以說是國賊。不過，事實上運作國家的不是國王，而是貴族官僚。就算這是不法的行為，透過賄賂也依然可以輕易地消掉踪跡。

「真是的……真是頭痛的問題。」

「我想，乾脆就給老師擔任首席，管理魔導士就可以了……」

「杰羅斯先生應該不會做那種事吧。和他敵對的話，也不知道會變得怎麼樣。」

對救命恩人強加重責大任是很不敬的。既然他都說「想在安靜的土地上隱居」，他們就無法強迫杰羅斯。不過，杰羅斯的評價正急速上升中。

前公爵想著國家的未來，對魔導士們的派系改革傷透腦筋。

本來和孫女應該要很開心的對話，卻像這樣連結到了政治性的話題。

隱居的爺爺好像無法徹底擺脫職業病。

順道一提，杰羅斯在別院庭院裡的農田努力務農，結束了今天這天。

因為大叔原本就是出於興趣務農。